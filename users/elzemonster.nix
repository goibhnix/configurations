{config, pkgs, ...}:
let
  packageGroups = import ../package-groups.nix {inherit pkgs;};
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};

  # bacula-dir-name = "bunnicula-dir";
  # bacula-dir-pw = "${builtins.readFile ./bacula/secrets/dir.pw}";
  # bacula-sd-pw = "${builtins.readFile ./bacula/secrets/sd.pw}";
  # bacula-fd-pw = "${builtins.readFile ./bacula/secrets/fd.pw}";
in

common.misc // {
  require = [ ../hardware/intel-nvidia-desktop.nix ];

  boot =  {
    loader.grub = common.grub // {
      device = "/dev/sda";
    };
    # blacklistedKernelModules = [ "acpi_cpufreq" "cpufreq_stats" "nvidiafb" ];
    kernelPackages = pkgs.linuxPackages // {
       #virtualbox = pkgs.linuxPackages.virtualbox.override {
       #  enableExtensionPack = true;
       #};
    };
  };

  fileSystems = {
    "/" =  {
      label = "monster";
      options = ["defaults" "noatime" "discard"];
    };
    "/media/Slow" =  {
      label = "slow";
      fsType = "xfs";
      options = ["defaults" "noatime" "discard"];
    };

  };

  swapDevices = [{ label = "swap"; }];

  hardware = {
    pulseaudio = common.pulseaudio; # // audio.pulseaudio;
    #sane.enable = true;
    bluetooth.enable = true;
  };

  powerManagement = common.powerManagement // {
    # Stop the disk getting parked, it reduces the lifespan
    powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -B 255 /dev/sdb";
  };

  environment = {
    systemPackages = with packageGroups;
       desktop ++ goibhnix ++ internet ++ videoProduction ++ multimediaConsumption  ++ graphicsProduction ++ userUtils ++ compressionUtils ++ 
serverUtils ++ kde5Packages ++ [ pkgs.corefonts pkgs.megatools ];
    pathsToLink = [ "/${pkgs.gimp.name}-plugins" ];
    # etc."bacula/bconsole.conf".text = ''
    #   Director {
    #     Name = bunnicula-dir
    #     address = localhost
    #     password = ${bacula-dir-pw}
    #   }
    # '';
    shellAliases = common.shellAliases;
  };

  programs.bash = common.bash;
  programs.ssh.startAgent = false;

  networking = common.networking // {
    hostName = "needle";
    firewall.allowedTCPPorts = [ 21 ];
    firewall.allowedTCPPortRanges = [ {from = 1714; to = 1764;}];
    firewall.allowedUDPPortRanges = [ {from = 1714; to = 1764;}];
    networkmanager.enable = true;
  };

  nixpkgs.config = common.config // {
    #sane.snapscanFirmware = /firmware/esfw41.bin;
  };

  fonts.fonts = common.fonts;
  virtualisation.virtualbox.host.enableExtensionPack = true;
  virtualisation.virtualbox.host.enable = true;
  virtualisation.virtualbox.guest.enable = true;

  services = common.services // audio.services // {
	pcscd.enable = true;
	#pcscd.plugins = [ pkgs.gppcscconnectionplugin pkgs.ccid pkgs.acsccid ];
    # bacula-dir = {
    #   enable = true;
    #   name = bacula-dir-name;
    #   password = bacula-dir-pw;
    #   extraDirectorConfig = ''
    #       Maximum Concurrent Jobs = 1
    #   '';
    #   extraConfig = "${builtins.readFile ./bacula/dir.jobs}" + ''
    #   # Client (File Services) to backup
    #     Client {
    #       Name = bunnicula-fd
    #       Address = localhost
    #       FDPort = 9102
    #       Catalog = PostgreSQL
    #       Password = ${bacula-fd-pw}          # password for FileDaemon
    #       File Retention = 30 days            # 30 days
    #       Job Retention = 6 months            # six months
    #       AutoPrune = yes                     # Prune expired Jobs/Files
    #     }

    # 	# Definition of file storage device
    #     Storage {
    #       Name = ElzemiekeStorage
    #       # Do not use "localhost" here
    #       Address = localhost                # N.B. Use a fully qualified name here
    #       SDPort = 9103
    #       Password = ${bacula-sd-pw}
    #       Device = ElzemiekeDev
    #       Media Type = File
    #     }

    #   '';
    # };

    # bacula-fd = {
    #   enable = true;
    #   name = "bunnicula-fd";
    #   director."bunnicula-dir" = {
    #     password = bacula-fd-pw;
    #   };

    #   extraClientConfig = ''
    #     Maximum Concurrent Jobs = 20
    #     #FDAddress = 127.0.0.1
    #   '';

    #   extraMessagesConfig = ''
    #     director = bunnicula-dir = all, !skipped, !restored
    #   '';

    # };

    # bacula-sd = {
    #   enable = true;
    #   name = "bunnicula-sd";
    #   device."ElzemiekeDev" = {
    #     archiveDevice = "/media/Slow/Backup/Bacula";
    # 	mediaType = "File";
    # 	extraDeviceConfig = ''
    # 	  LabelMedia = yes;                   # lets Bacula label unlabeled media
    # 	  Random Access = Yes;
    # 	  AutomaticMount = yes;               # when device opened, read it
    # 	  RemovableMedia = no;
    # 	  AlwaysOpen = no;
    # 	  LabelMedia = yes
    # 	'';
    #   };
    #   director."bunnicula-dir" = {
    #     password = bacula-sd-pw;
    #   };
    #   extraMessagesConfig = ''
    #     #Name = Standard
    #     director = bunnicula-dir = all
    #   '';
    # };

    # postgresql = {
    #   enable = true;
    #   package = pkgs.postgresql93;
    #   authentication = pkgs.lib.mkForce ''
    # # Generated file; do not edit!
    # # TYPE  DATABASE        USER            ADDRESS                 METHOD
    # local   all             all                                     trust
    # host    all             all             127.0.0.1/32            trust
    # host    all             all             ::1/128                 trust
    # '';

    # };
    xserver = {
      desktopManager.plasma5.enable = true;
      displayManager = {sddm.enable = true; defaultSession = "plasma5";};
      enable = true;
      layout = "gb";
      wacom.enable = true;
      libinput.enable = true;
      videoDrivers = ["vesa"  "intel"];
    };

    printing = {
      enable = true;
      drivers = [ pkgs.gutenprint ];
    };

    avahi.enable = true;
  };
  users.extraGroups.vboxusers.members = [ "elzemieke" ];
  nix.extraOptions = "trusted-binary-caches = [http://cache.nixos.org http://hydra.nixos.org]";
  time.timeZone = null;
}

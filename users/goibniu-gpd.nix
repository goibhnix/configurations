{ config, pkgs, ... }:
let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../package-groups.nix {
    inherit pkgs;
  };
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};
  server = import ../server.nix {inherit pkgs;};
  steamPa = { support32Bit = true; };
in

{
  imports =
    [
      ../hardware/gpd-win-max-2.nix
    ];

    # Use the systemd-boot EFI boot loader.
    boot = common.boot // {
      loader.systemd-boot.enable = true;
      loader.efi.canTouchEfiVariables = true;
      initrd.kernelModules = [ "amdgpu" ];
      kernelParams = ["amdgpu.sg_display=0"];
      #kernelPackages = pkgs.linuxPackages_latest; # for working hibernation
    };

  hardware = {
    steam-hardware.enable = true;
    bluetooth.enable = true;
    enableAllFirmware = true;
    opengl.driSupport32Bit = true;
    pulseaudio = common.pulseaudio // audio.pulseaudio // steamPa;
  };
  powerManagement.enable = true;
  nixpkgs.config = common.config // audio.config // {
    firefox = {
      enablePlasmaBrowserIntegration = true;
    };
    kodi.enablePVRHTS = true;
    permittedInsecurePackages = [
      "libgcrypt-1.8.10" # for gpg22
    ];                          
  };

  services = audio.services // {
    # dockerRegistry.enable = true;
    avahi.enable = true;
    avahi.nssmdns = true;
    # softether.vpnclient.enable = true;
    acpid.enable = true;
    locate.enable = true;
    journald.extraConfig = "SystemMaxUse=256M";
    openssh.enable = true;
    # power-profiles-daemon.enable = false;
    # tlp = {
    #     enable = true;
    #     settings = {
    #         START_CHARGE_THRESH_BAT0=75;
    #         STOP_CHARGE_THRESH_BAT0=80;
    #     };
    # };
    xserver = {
      desktopManager.plasma5.enable = true;
      displayManager = {sddm.enable = true; defaultSession = "plasma";};
      videoDrivers = [ "amdgpu" ];
      enable = true;
      layout = "gb";
      libinput.enable = true;
      wacom.enable = true;
     };
  };

  environment = {
    systemPackages = with packageGroups;
     desktop ++ desktop64 ++ dev ++ internet ++ multimediaConsumption ++
     networking ++ text ++ utilsFull ++ multimediaProduction ++ [(pkgs.kodi.passthru.withPackages(kodiPkgs: with kodiPkgs; [ pvr-hts youtube steam-controller inputstream-adaptive inputstream-ffmpegdirect inputstreamhelper invidious ]))] ++
     [
       pkgs.ansible
       pkgs.arduino
       pkgs.chromedriver
       pkgs.esptool-ck
       pkgs.feh
       pkgs.ffmpeg
       pkgs.jamulus
       pkgs.i2c-tools
       # pkgs.libreoffice
       pkgs.minicom
       pkgs.mosquitto
       pkgs.peek
       pkgs.pre-commit
       pkgs.signal-desktop
       pkgs.sshuttle
       pkgs.yarn
       pkgs.cpufrequtils
       pkgs.geckodriver
       pkgs.mattermost-desktop
       pkgs.obs-studio
       pkgs.pwgen
       pkgs.python3Packages.flake8
       pkgs.python3Packages.black
       pkgs.qt5.qttools
     ];   #pkgs.blender pkgs.steam  blenderCuda pkgs.audacious  ffmpegNvenc
    shellAliases = common.shellAliases;
    pathsToLink = [ "/${pkgs.gimp.name}-plugins" ];
    shellInit = audio.shellInit;
  };

  programs.bash = common.bash;
  programs.gnupg.agent.enable = true;
  programs.gnupg.agent.enableSSHSupport = true;
  programs.adb.enable = true;
  programs.dconf.enable = true;
  programs.extra-container.enable = true;
  fonts.fontconfig.subpixel.rgba = "vbgr";
  fonts.fontconfig.subpixel.lcdfilter = "light";
  # fonts.fontconfig.subpixel.lcdfilter = "legacy";
  console.keyMap = "uk";
  i18n.defaultLocale = "en_GB.UTF-8";
  networking = {
    networkmanager.unmanaged = [
      "interface-name:ve-*"
    ];
    firewall.allowedTCPPorts = [ 21 22 80 443 5353 8000 8080 3819 2021 ];
    firewall.allowedUDPPorts = [ 3819 2021 ];
    firewall.allowedTCPPortRanges = [
      { from = 1714; to = 1764; } # kdeconnect
    ];
    firewall.allowedUDPPortRanges = [
      { from = 1714; to = 1764; }
    ];
    enableIPv6 = true;
    networkmanager.enable = true;
    hostName = "gpd";
    nat = {
      enable = true;
      internalInterfaces = ["ve-+"];
      externalInterface =  "wlp1s0";
    };
    # extraHosts = ''
    # 137.193.6.142 events.unibw.de
    # '';
  };
  time.timeZone = "Europe/Dublin";
  users.extraUsers.goibhniu = {
    uid = 1000;
    home = "/home/goibhniu";
    shell = "/run/current-system/sw/bin/bash";
    extraGroups = [ "wheel" "audio" "adbusers"];
  };
  users.users.goibhniu.isNormalUser = true;

  fonts.fonts = common.fonts;

  security.pam.loginLimits = audio.loginLimits;
  nix.extraOptions = "binary-caches-parallel-connections = 10";
  nix.settings = {
    trusted-public-keys = [ "hydra.nixos.org-1:CNHJZBh9K4tP3EKF6FkkgeVYsS3ohTl+oS0Qa8bezVs="  "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];
    sandbox = true;
    experimental-features = [ "nix-command" "flakes" ];
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}

{config, pkgs, ...}:
let
  packageGroups = import ../package-groups.nix {inherit pkgs;};
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};

in

common.misc // {
  require = [ ../hardware/x1.nix ];

  boot =  {
    loader.grub = common.grub // {
      device = "/dev/sda";
      useOSProber = true;
    };

    kernelPackages = pkgs.linuxPackages_latest // {
      # virtualbox = pkgs.linuxPackages.virtualbox.override {
      #   enableExtensionPack = true;
      # };
    };
  };

  hardware = {
    pulseaudio = {
      enable = true;
    };
  };

  powerManagement = common.powerManagement;

  environment = {
    systemPackages = with packageGroups;
       desktop ++ goibhnix ++ internet ++ text ++ compressionUtils ++ kde5Packages ++ multimediaFull;
    pathsToLink = [ "/${pkgs.gimp.name}-plugins" ];
    shellAliases = common.shellAliases;
  };

  programs.bash = common.bash;

  networking = common.networking // {
    hostName = "x1";
  };

  nixpkgs.config = common.config;

  fonts.fonts = common.fonts;

  services = common.services // audio.services // {
    xserver = {
      desktopManager = {default = "plasma5"; plasma5.enable = true;};
      displayManager = {sddm.enable = true; slim.enable = false;};
      enable = true;
      layout = "gb";
      libinput = {
        enable = true;
        disableWhileTyping = true;
      };
    };

    printing = {
      enable = true;
      drivers = [ pkgs.gutenprint ];
    };

    avahi.enable = true;
    tlp.enable = true;
  };

}

{ config, pkgs, ... }:
let
  mqttPassword = import ../secrets/mqttPassword.nix;
in
{
  imports =
    [
      ../hardware/nead.nix
    ];
  system.stateVersion = "21.11";

  boot = {
    kernelPackages = pkgs.linuxPackages_rpi4;
    # tmpOnTmpfs = true; # See note
    kernelParams = [
      "8250.nr_uarts=1"
      "console=ttyAMA0,115200"
      "console=tty1"
      "cma=128M"
    ];
    loader = {
      raspberryPi = {
        enable = true;
        version = 4;
      };
      grub.enable = false;
    };
    cleanTmpDir = true;
  };

  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 30d";

  hardware.enableRedistributableFirmware = true;
  nixpkgs.config = {
    allowUnfree = true;
  };

  networking.hostName = "nead";

  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Dublin";

  networking.firewall.allowedTCPPorts = [ 80 443 8123 1883 ];

  i18n.defaultLocale = "en_GB.UTF-8";
  console.keyMap = "uk";

  users.users.goibhniu = {
     isNormalUser = true;
     extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

  environment.systemPackages = with pkgs; [
    #libraspberrypi
    mosquitto
    xfsprogs
  ];

  services = {
    openssh = {
      enable = true;
    };
    home-assistant = {
      enable = true;
      # package = hass;
      openFirewall = true;
      configDir = "/srv/hass";
      config = {
        homeassistant = {
          name = "Abhaile";
          time_zone = "UTC";
          latitude = "52.9428183";
          longitude = "-8.0400542";
          elevation = "380";
        };
        default_config = "";
        mobile_app = "";
        mqtt = {
          broker = "192.168.178.24";
          username = "homeassistant";
          password = mqttPassword;
          discovery = true;
        };
        light = [{
          platform = "mqtt";
          schema = "template";
          name = "Teolaí";
          command_topic = "shellies/shellyrgbw2-6D7CCE/white/2/set";
          state_topic = "shellies/shellyrgbw2-6D7CCE/white/2/status";
          command_on_template = ''
            {"turn": "on"
            {%- if brightness is defined -%}
            , "brightness": {{brightness | float | multiply(0.3922) | round(0)}}
            {%- endif -%}
            {%- if red is defined and green is defined and blue is defined -%}
            , "red": {{ red }}, "green": {{ green }}, "blue": {{ blue }}
            {%- endif -%}
            {%- if white_value is defined -%}
            , "white": {{ white_value }}
            {%- endif -%}
            {%- if effect is defined -%}
            , "effect": {{ effect }}
            {%- endif -%}
          '';
          command_off_template = ''{"turn":"off"}'';
          state_template = "{% if value_json.ison %}on{% else %}off{% endif %}";
          brightness_template = "{{ value_json.brightness | float | multiply(2.55) | round(0) }}";
          qos = 2;
        }
        {
          platform = "mqtt";
          schema = "template";
          name = "Úr";
          command_topic = "shellies/shellyrgbw2-6D7CCE/white/3/set";
          state_topic = "shellies/shellyrgbw2-6D7CCE/white/3/status";
          command_on_template = ''
            {"turn": "on"
            {%- if brightness is defined -%}
            , "brightness": {{brightness | float | multiply(0.3922) | round(0)}}
            {%- endif -%}
            {%- if red is defined and green is defined and blue is defined -%}
            , "red": {{ red }}, "green": {{ green }}, "blue": {{ blue }}
            {%- endif -%}
            {%- if white_value is defined -%}
            , "white": {{ white_value }}
            {%- endif -%}
            {%- if effect is defined -%}
            , "effect": {{ effect }}
            {%- endif -%}
          '';
          command_off_template = ''{"turn":"off"}'';
          state_template = "{% if value_json.ison %}on{% else %}off{% endif %}";
          brightness_template = "{{ value_json.brightness | float | multiply(2.55) | round(0) }}";
          qos = 2;
        }];
        switch = [{
          platform = "mqtt";
          name = "Leaba";
          command_topic = "shellies/shelly1pm-F30A5F/relay/0/command";
          state_topic = "shellies/shelly1pm-F30A5F/relay/0";
          payload_on = "on";
          payload_off = "off";
          retain = false;
        } {
           platform = "mqtt";
           name = "Fuaim";
           command_topic = "shellies/shelly1pm-F27204/relay/0/command";
           state_topic = "shellies/shelly1pm-F27204/relay/0";
           payload_on = "on";
           payload_off = "off";
           retain = false;
        }];
        sensor = [{
          platform = "mqtt";
          name = "Cumhacht";
          state_topic = "shellies/shelly1pm-F30A5F/relay/0/power";
          unit_of_measurement = "Watts";
        } {
           platform = "mqtt";
           name = "Fuinneamh";
           state_topic = "shellies/shelly1pm-F30A5F/relay/0/energy" ;
           unit_of_measurement = "kWh";
        }
        {
           platform = "mqtt";
           name = "Leaba: Teocht A";
           state_topic = "shellies/shelly1pm-F30A5F/ext_temperature/0" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Leaba: Teocht B";
           state_topic = "shellies/shelly1pm-F30A5F/ext_temperature/1" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Siopa: teocht";
           state_topic = "homeassistant/sensor/fa1173070020/teocht-an-siopa/value" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Siopa: bogthaise";
           state_topic = "homeassistant/sensor/fa1173070020/bogthaise-an-siopa/value" ;
           unit_of_measurement = "RH";
        }
        {
           platform = "mqtt";
           name = "Siopa: brú";
           state_topic = "homeassistant/sensor/fa1173070020/bru-an-siopa/value" ;
           unit_of_measurement = "hPa";
        }
        {
          platform = "dewpoint";
          name = "Siopa: Drúcht";
          temp_sensor = "sensor.teocht_an_siopa_4";
          humidity_sensor = "sensor.bogthaise_an_siopa";
        }
        {
           platform = "mqtt";
           name = "Seomra Thuas: teocht";
           state_topic = "homeassistant/sensor/fa1173070021/teocht-an-seomra-thuas/value" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Seomra Thuas: bogthaise";
           state_topic = "homeassistant/sensor/fa1173070021/bogthaise-an-seomra-thuas/value" ;
           unit_of_measurement = "%";
        }
        {
           platform = "mqtt";
           name = "Seomra Thuas: brú";
           state_topic = "homeassistant/sensor/fa1173070021/bru-an-seomra-thuas/value" ;
           unit_of_measurement = "hPa";
        }
        {
          platform = "dewpoint";
          name = "Seomra Thuas: Drúcht";
          temp_sensor = "sensor.teocht_an_seomra_thuas";
          humidity_sensor = "sensor.bogthaise_an_seomra_thuas";
        }
        {
           platform = "mqtt";
           name = "Dánlann: teocht";
           state_topic = "homeassistant/sensor/fa1173070022/teocht-an-danlann/value" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Dánlann: bogthaise";
           state_topic = "homeassistant/sensor/fa1173070022/bogthaise-an-danlann/value" ;
           unit_of_measurement = "RH";
        }
        {
           platform = "mqtt";
           name = "Dánlann: brú";
           state_topic = "homeassistant/sensor/fa1173070022/bru-an-danlann/value" ;
           unit_of_measurement = "hPa";
        }
        {
          platform = "dewpoint";
          name = "Dánlann: Drúcht";
          temp_sensor = "sensor.teocht_an_danlann";
          humidity_sensor = "sensor.bogthaise_an_danlann";
        }
        {
           platform = "mqtt";
           name = "Taobh Amuigh: teocht";
           state_topic = "homeassistant/sensor/fa1173070023/bru-taobh-amuigh/value" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Taobh Amuigh: bogthaise";
           state_topic = "homeassistant/sensor/fa1173070023/bru-taobh-amuigh/value" ;
           unit_of_measurement = "RH";
        }
        {
           platform = "mqtt";
           name = "Taobh Amuigh: brú";
           state_topic = "homeassistant/sensor/fa1173070023/bru-taobh-amuigh/value" ;
           unit_of_measurement = "hPa";
        }
        {
          platform = "dewpoint";
          name = "Taobh Amuigh: Drúcht";
          temp_sensor = "sensor.teocht_taobh_amuigh";
          humidity_sensor = "sensor.bogthaise_taobh_amuigh";
        }
        {
           platform = "mqtt";
           name = "An Chistin: teocht";
           state_topic = "homeassistant/sensor/fa1173070024/bru-an-chistin/value" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "An Chistin: bogthaise";
           state_topic = "homeassistant/sensor/fa1173070024/bru-an-chistin/value" ;
           unit_of_measurement = "RH";
        }
        {
           platform = "mqtt";
           name = "An Chistin: brú";
           state_topic = "homeassistant/sensor/fa1173070024/bru-an-chistin/value" ;
           unit_of_measurement = "hPa";
        }
        {
          platform = "dewpoint";
          name = "An Chistin: Drúcht";
          temp_sensor = "sensor.teocht_an_chistin";
          humidity_sensor = "sensor.bogthaise_an_chistin";
        }
        {
           platform = "mqtt";
           name = "Taobh Amuigh: teocht";
           state_topic = "homeassistant/sensor/fa1173070023/bru-taobh-amuigh/value" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Taobh Amuigh: bogthaise";
           state_topic = "homeassistant/sensor/fa1173070023/bru-taobh-amuigh/value" ;
           unit_of_measurement = "RH";
        }
        {
           platform = "mqtt";
           name = "Taobh Amuigh: brú";
           state_topic = "homeassistant/sensor/fa1173070023/bru-taobh-amuigh/value" ;
           unit_of_measurement = "hPa";
        }
        {
           platform = "mqtt";
           name = "Gairdín: teocht";
           state_topic = "shellies/shellyht-F38A8E/sensor/temperature" ;
           unit_of_measurement = "C";
        }
        {
           platform = "mqtt";
           name = "Gairdín: bogthaise (f)";
           state_topic = "shellies/shellyht-F38A8E/sensor/humidity" ;
           unit_of_measurement = "RH";
        }
        {
           platform = "mqtt";
           name = "Gairdín: fuinneamh";
           state_topic = "shellies/shelly1pm-F27204/relay/0/energy";
           unit_of_measurement = "kWh";
        }
        {
          platform = "mqtt";
          name = "Gairdín: cumhacht";
          state_topic = "shellies/shelly1pm-F27204/relay/0/power";
          unit_of_measurement = "Watts";
        }
        ];
        climate = [{
            platform = "generic_thermostat";
            name = "Cnaipe an leaba";
            # ac_mode = true;
            # min_cycle_duration = "";
            heater = "switch.leaba";
            target_sensor = "sensor.leaba_teocht_a";
            target_temp = "20";
        }];
        recorder = {
          include = {
            entity_globs = [
              "sensor.*bogthaise*"
              "sensor.*teocht*"
              "sensor.*bru*"
              "sensor.*cumhacht*"
              "sensor.*fuinneamh*"
              "sensor.*drucht*"
            ];
          };
          commit_interval = 1800;
          purge_keep_days = 600;
        };
      };
    };
    mosquitto = {
      enable = true;
      listeners = [{
        address = "192.168.178.24";
        users = {
          homeassistant = {
            acl = [ "readwrite #" ];
            password = mqttPassword;
          };
        };
      }];
    };

    nextcloud = {
      enable = true;
      hostName = "xn--nal-bma.xn--nead-na-bhfinleog-hpb.ie";
      package = pkgs.nextcloud25;
      # home = "/var/www/nextcloud";
      datadir = "/srv/nextcloud";
      https = true;
      autoUpdateApps.enable = true;
      autoUpdateApps.startAt = "05:00:00";
      config = {
        overwriteProtocol = "https";
        dbtype = "pgsql";
        dbname = "nextcloud";
        dbhost = "/run/postgresql";
        dbport = 5432;
        dbuser = "nextcloud";
        dbpassFile = "/etc/nixos/secrets/nextcloud_db";
        adminuser = "cinnire";
        adminpassFile = "/etc/nixos/secrets/nextcloud_adminpass";
        extraTrustedDomains = [
          "k2p0vrjzprfhfubn.myfritz.net"
        ];
      };
    };

    # Enable PostgreSQL
    postgresql = {
      enable = true;

      # Ensure the database, user, and permissions always exist
      ensureDatabases = [ "nextcloud" ];
      ensureUsers = [
        { name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
        }
      ];
    };
    # postgresql = {
    #   enable = true;
    #   package = pkgs.postgresql_10;
    #   authentication = lib.mkForce ''
    #     # "local" is for Unix domain socket connections only
    #     local   all             all                                     trust
    #     local   all             all                                     md5
    #     # IPv4 local connections:
    #     host    all             all             127.0.0.1/32            trust
    #     # IPv6 local connections:
    #     host    all             all             ::1/128                 trust
    #     # Allow replication connections from localhost, by a user with the
    #     # replication privilege.
    #     local   replication     all                                     trust
    #     host    replication     all             127.0.0.1/32            trust
    #     host    replication     all             ::1/128                 trust
    #   '';
    # };
    nginx = {
      enable = true;

      # Use recommended settings
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      # Only allow PFS-enabled ciphers with AES256
      sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";

      # https://lala.im/8462.html
      upstreams."peertube-backend" = {
        servers = {
          "127.0.0.1:9000" = {};
        };
      };
      virtualHosts = {
        # "scamall.cillian.org" = {
        #   forceSSL = true;
        #   enableACME = true;
        # };
        # "k2p0vrjzprfhfubn.myfritz.net" = {
        #   #forceSSL = true;
        #   #enableACME = true;
        # };
        # "rpi" = {
        #   #forceSSL = true;
        #   #enableACME = true;
        # };

        "xn--nal-bma.xn--nead-na-bhfinleog-hpb.ie" = {
          ## Force HTTP redirect to HTTPS
          forceSSL = true;
          ## LetsEncrypt
          enableACME = true;
        };
        "www.xn--nead-na-bhfinleog-hpb.ie" = {
          ## Force HTTP redirect to HTTPS
          forceSSL = true;
          ## LetsEncrypt
          enableACME = true;
          root = "/srv/www";
          locations."/" = {
            extraConfig = ''
              autoindex on;
            '';
          };
        };
        "xn--fsein-zqa5f.xn--nead-na-bhfinleog-hpb.ie" = {
          enableACME = true;
          forceSSL = true;
          kTLS = true;
          locations."/" = {
            proxyPass = "http://peertube-backend";
            proxyWebsockets = true;
            extraConfig = ''
              client_max_body_size 0;
            '';
          };
        };
      };
    };
    peertube = {
      enable = true;
      localDomain = "xn--fsein-zqa5f.xn--nead-na-bhfinleog-hpb.ie";
      listenWeb = 443;
      enableWebHttps = true;
      database.createLocally = true;
      redis.createLocally = true;
      serviceEnvironmentFile = "/etc/peertube-password-init-root";
      secrets.secretsFile = /home/goibhniu/secrets/peertube;
      settings = {
        import = {
          videos = {
            http = {
              youtube_dl_release = {
                url = "https://api.github.com/repos/yt-dlp/yt-dlp/releases";
                name = "yt-dlp";
              };
            };
          };
        };
      };
    };
  };
  
  security.acme.acceptTerms = true;
  security.acme.defaults.email = "goibhniu@fsfe.org";
  # Ensure that postgres is running before running the setup
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };
  # systemd.tmpfiles.rules = [
  #   "C /var/lib/hass/custom_components/sensor - - - - /srv/hass/custom_components/homeassistant-dewpoint/custom_components/sensor"
  #   "Z /var/lib/hass/custom_components/sensor 770 hass hass - -"
  # ];

}

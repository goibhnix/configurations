{ config, pkgs, ... }:
let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../package-groups.nix {inherit pkgs;};
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};
in

common.misc // {
  require = [ ../hardware/lenovo-S10e.nix ];

  boot = common.boot // {
    loader.grub = common.grub // { device = "/dev/sda";};
    postBootCommands = audio.postBootCommands;
  };

  fileSystems = [ { mountPoint = "/"; device = "/dev/disk/by-label/root";} ];
  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  hardware = {
    enableAllFirmware = true;
    pulseaudio = common.pulseaudio;
  };

  powerManagement = common.powerManagement // {
    # Stop the disk getting parked, it reduces the lifespan
    powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -B 255 /dev/sda";
  };

  environment = {
    systemPackages = with packageGroups;
      [ desktop dev goibhnix internet multimediaFull pkgs.jack2
        networking python text utilsFull ];
    shellInit = common.shellInit + audio.shellInit;
  };

  networking = common.networking // { hostName = "sloe"; };

  nixpkgs.config = common.config;
  services = common.services // { xserver.windowManager.fluxbox.enable = true; };
  fonts.fonts = common.fonts;

}

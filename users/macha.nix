{config, pkgs, ...}:
let
  packageGroups = import ../package-groups.nix {inherit pkgs;};
  server = import ../server.nix {inherit pkgs;};
in

server.misc // {
  require = [ ../hardware/netcup-neptun.nix ];

  environment = {
    systemPackages = with packageGroups;
      goibhnix ++ serverUtils ++ compressionUtils ++ vcs ++ zotonic;
  };

  networking = {
    hostName = "macha";
    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 443 2222 8000 8443 ];
      extraCommands = ''
        iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to 8000
        iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to 8443
        iptables -t nat -A OUTPUT -p tcp -d 37.221.197.163 --dport 80 -j REDIRECT --to 8000
        iptables -t nat -A OUTPUT -p tcp -d 37.221.197.163 --dport 443 -j REDIRECT --to 8443
      '';
    };
  };

  services = server.services;
}


# the system.  Help is available in the configuration.nix(5) man page
# or the NixOS manual available on virtual console 8 (Alt+F8).

{ config, pkgs, ... }:
let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../package-groups.nix {
    inherit pkgs;
  };
in
{
  require =
    [ # Include the results of the hardware scan.
      ../hardware/ASUS-A6Tc.nix
    ];

  boot = {
    vesa = false;
    kernelPackages = pkgs.linuxPackages_3_4;
    resumeDevice = "8:1";
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
      extraEntries = ''
        menuentry "Linux Mint" {
        set root=(hd0,5)
        linux /vmlinuz root=/dev/sda5 
        initrd /initrd.img
        }
      '';
    };
    postBootCommands = ''
      ${pkgs.procps}/sbin/sysctl -w vm.swappiness=10
    '';
  };

  # Add filesystem entries for each partition that you want to see
  # mounted at boot time.  This should include at least the root
  # filesystem.
  fileSystems =
    [  { mountPoint = "/";
         device = "/dev/disk/by-label/nixos";
       }

       { mountPoint = "/home"; 
         device = "/dev/disk/by-label/home";
       }

       { mountPoint = "/mnt/storage"; 
         device = "/dev/disk/by-label/storage";
       }

    ];

  # List swap partitions activated at boot time.
  swapDevices =
    [  { device = "/dev/sda1"; }
    ];

  hardware.pulseaudio.enable = true;
  powerManagement = {
    cpuFreqGovernor = "ondemand";
    enable = true;
    # Stop the disk getting parked, it reduces the lifespan
    powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -B 255 /dev/sda";
  };
  

### config options
  nixpkgs.config = {
    pulseaudio = true;
    firefox.enableGoogleTalkPlugin = true;
  };

### Software, services
  jobs.dhcpcd.startOn = pkgs.lib.mkOverride 10 "";

  services = {
    #nscd.enable = false;
    acpid.enable = true;
    cron.systemCronJobs = [ "30 1 * * * root xfs_fsr -t 21600 >/dev/null 2>&1" ];
    dbus.packages = [ pkgs.gnome.GConf ];
    locate.enable = true;
    openssh.enable = true;
    printing.enable = true;
    xserver = {
      desktopManager = {default = "kde4"; kde4.enable = true;};
      displayManager = {kdm.enable = true; slim.enable = false;};
      enable = true;
      layout = "gb";
      synaptics.enable = true;
    };
  };

  environment = {
    systemPackages = with packageGroups;
      [ desktop goibhnix graphicsProduction internet
        multimediaConsumption text userUtils videoProduction ];
    shellInit = ''
      export GTK_PATH=$GTK_PATH:/var/run/current-system/sw/lib/gtk-2.0
      export GTK2_RC_FILES=$GTK2_RC_FILES:/var/run/current-system/sw/share/themes/oxygen-gtk/gtk-2.0/gtkrc
    '';
  };

  fonts.enableCoreFonts = true;
  i18n = {consoleKeyMap = "uk"; defaultLocale = "en_GB.UTF-8";};
  networking = {
    hostName = "nixos";
    wicd.enable = true;
    extraHosts = ''
    '';
    enableB43Firmware = true;
  };
  time.timeZone = "Europe/Berlin";
}

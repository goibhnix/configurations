{config, pkgs, ...}:
let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../package-groups.nix {inherit pkgs;};
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};
in

common.misc // {
  require = [ ../hardware/ThinkPad-T500.nix ];

  boot = common.boot // {
    loader.grub = common.grub // { device = "/dev/sda";};
    # postBootCommands = audio.postBootCommands;
  };

  fileSystems."/".label = "nixos";
  fileSystems."/mnt/bulk".label = "bulk";
  fileSystems."/var/lib/containers/plone/etc/nixos/goibhnix" = {
    device = "/home/goibhniu/.config/goibhnix/configurations";
    options = "bind";
  };
  fileSystems."/var/lib/containers/plone/srv/plone-dev" = {
    device = "/srv/plone-dev";
    options = "bind";
  };
  fileSystems."/var/lib/containers/plone/home/goibhniu" = {
    device = "/home/goibhniu";
    options = "bind";
  };

  # fileSystems."/var/lib/containers/star/etc/nixos/goibhnix" = {
  #   device = "/home/goibhniu/.config/goibhnix/configurations";
  #   options = "bind";
  # };
  # fileSystems."/var/lib/containers/star/srv/jochi" = {
  #   device = "/home/goibhniu/Syslab/clients/star/jochi";
  #   options = "bind";
  # };
  # fileSystems."/var/lib/containers/mw/etc/nixos/config" = {
  #   device = "/home/goibhniu/.config/goibhnix/configurations";
  #   options = "bind";
  # };

  # fileSystems."/var/lib/containers/mw2/etc/nixos/config" = {
  #   device = "/home/goibhniu/.config/goibhnix/configurations";
  #   options = "bind";
  # };

  # fileSystems."/var/lib/containers/screencast/etc/nixos/config" = {
  #   device = "/home/goibhniu/.config/goibhnix/configurations";
  #   options = "bind";
  # };

  # fileSystems."/var/lib/containers/elzemieke/etc/nixos/config" = {
  #   device = "/home/goibhniu/.config/goibhnix/configurations";
  #   options = "bind";
  # };

  # fileSystems."/var/lib/containers/elzemieke/srv/zotonic" = {
  #   device = "/home/goibhniu/Projects/Haxoring/Zotonic/";
  #   options = "bind";
  # };

  swapDevices = [{ device = "/dev/sdb1"; }];

  hardware = {
    pulseaudio = common.pulseaudio // audio.pulseaudio;
    #sane.enable = true;
  };

  powerManagement = common.powerManagement // audio.powerManagement // {
    # Stop the disk getting parked, it reduces the lifespan
   # powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -B 255 /dev/sda";
  };

  environment = {
    systemPackages = with packageGroups; 
      builtins.concatLists [
         desktop dev goibhnix internet multimediaFull 
        networking screencasting text utilsFull
      ] ++ 
      [ pkgs.gnucash pkgs.bitcoin ]; #jackaudioFirewire
    shellInit = audio.shellInit;
    pathsToLink = [ "/${pkgs.gimp.name}-plugins" ];
  }; #audioFirewire

  programs.bash = common.bash;
  programs.ssh.startAgent = false;

  networking = common.networking // {
    hostName = "goibhniu"; 
    extraHosts = ''
      127.0.0.1 goibhniu.eu goibhniu zot09 
      10.233.6.2 screenhome screenwiki
      10.233.7.2 elzemieke elzemieke.org www.elzemieke.org
      10.233.1.2 plone
    '';

    nat = {
      enable = true;
      internalInterfaces = ["ve-+"];
      externalInterface = "wlp3s0";
    };
  };

  nixpkgs.config = common.config // audio.config // {
    #sane.snapscanFirmware = /firmware/esfw41.bin;
    allowUnfree = true;
#    virtualbox.enableExtensionPack = true;
  };

  # virtualisation.virtualbox.host.enable = true;
      
  services = common.services // audio.services // {
    postgresql = {
        enable = true;
        package = pkgs.postgresql92;
      };
    printing = {
      enable = true;
      drivers = [ pkgs.gutenprint ];
    };
    upower.enable = true;
    locate.period = "53 10 * * *";
    avahi.enable = true;
    udev.extraRules = ''
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="6089", MODE:="0666"
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="cc15", MODE:="0666"
    '';
  };

  security.pam.loginLimits = audio.loginLimits;

  fonts.fonts = common.fonts;

  nix.extraOptions = "binary-caches-parallel-connections = 10";
  nix.useSandbox = true;
  nix.binaryCachePublicKeys = [ "hydra.nixos.org-1:CNHJZBh9K4tP3EKF6FkkgeVYsS3ohTl+oS0Qa8bezVs=" ];

}


{ config, pkgs, ... }:
let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../package-groups.nix {inherit pkgs;};
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};
in

common.misc // {
  require = [ ../hardware/ASUS-EEE-1005HA.nix ];

  boot = common.boot // {
    loader.grub = common.grub // { device = "/dev/sda";};
  };

  fileSystems = [ { mountPoint = "/"; device = "/dev/disk/by-label/root";} ];
  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  hardware = {
    pulseaudio = common.pulseaudio;
  };

  powerManagement = common.powerManagement // {
    powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -B 255 /dev/sda";
  };

  environment = {
    systemPackages = with packageGroups;
      [ desktop goibhnix internet multimediaFull
        networking text utilsFull ];
    shellInit = common.shellInit;
  };

  networking = common.networking // { hostName = "loosha"; };

  fonts.fonts = common.fonts;

  nixpkgs.config = common.config;
  services = common.services;
}

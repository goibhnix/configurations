{config, pkgs, ...}:
let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../package-groups.nix {
    inherit pkgs;
  };
  common = import ../common.nix {inherit pkgs;};
  audio = import ../audio.nix {inherit pkgs;};
  server = import ../server.nix {inherit pkgs;};
  steamPa = { support32Bit = true; };

  containerBindMounts = name: {
    fileSystems."/var/lib/containers/${name}/etc/nixos/goibhnix" = {
      device = "/etc/goibhnix/configurations";
      options = [ "bind" ];
    };
    fileSystems."/var/lib/containers/${name}/srv/${name}" = {
      device = "/home/goibhniu/Syslab/clients/${name}/container";
      options = [ "bind" ];
    };
    fileSystems."/var/lib/containers/${name}/home/goibhniu" = {
      device = "/home/goibhniu";
      options = [ "bind" ];
    };
  };
in
{

  require = [
    ../hardware/AMD-desktop.nix
  ];

  #containerBindMounts "philrom";
  # imports = [
  #   ./containers/philrom/host.nix
  # ];

  boot = common.boot // {
    # plymouth.enable = true;
    loader.grub = common.grub // { device = "/dev/sda";};
    kernel.sysctl = {"kernel.sysrq" = 1;};
    extraModprobeConfig = ''
      options usb-storage quirks=174c:55aa:u 
    '';
  };

  # Add file system entries for each partition that you want to see mounted
  # at boot time.  You can add filesystems which are not mounted at boot by
  # adding the noauto option.
  fileSystems."/" = {
    # device = "/dev/sda2";
    label = "oficejo";
    options = [ "defaults" "noatime" "discard" ];
  };

  fileSystems."/var/lib/containers/denso-csr/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/denso-csr/srv/denso" = {
    device = "/home/goibhniu/Syslab/clients/denso/csr/container";
    options = [ "bind" ];
  };
  fileSystems."/srv/denso" = {
    device = "/home/goibhniu/Syslab/clients/denso/csr/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/denso-csr/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/denso-tec/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/denso-tec/srv/tec" = {
    device = "/srv/tec";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/denso-tec/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/hw2014/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/hw2014/srv/osha" = {
    device = "/home/goibhniu/Syslab/clients/osha/hw2014/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/hw2014/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/gfb/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/gfb/srv/gfb" = {
    device = "/home/goibhniu/Syslab/clients/gfb/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/gfb/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/philrom/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/philrom/srv/philrom" = {
    device = "/home/goibhniu/Syslab/clients/philrom/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/philrom/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };


  fileSystems."/var/lib/containers/pdfdiff/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/pdfdiff/srv/pdfdiff" = {
    device = "/home/goibhniu/Syslab/clients/denso/pdfdiff/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/pdfdiff/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/brandbook/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/brandbook/srv/brandbook" = {
    device = "/home/goibhniu/Syslab/clients/brandbook/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/brandbook/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/oiraproject/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/oiraproject/srv/oiraproject" = {
    device = "/home/goibhniu/Syslab/clients/oiraproject/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/oiraproject/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };
  fileSystems."/srv/oiraproject" = {
    device = "/var/lib/containers/oiraproject/srv/oiraproject";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/star/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/star/srv/star" = {
    device = "/home/goibhniu/Syslab/clients/star/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/star/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };
  fileSystems."/srv/star" = {
    device = "/home/goibhniu/Syslab/clients/star/container";
    options = [ "bind" ];
  };


  fileSystems."/var/lib/containers/eca/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/eca/srv/eca" = {
    device = "/home/goibhniu/Syslab/clients/eca/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/eca/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/plone-dev/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/plone-dev/srv/plone-dev" = {
    device = "/home/goibhniu/Syslab/clients/plone-dev/container";
    options = [ "bind" ];
  };
  fileSystems."/srv/plone-dev" = {
    device = "/home/goibhniu/Syslab/clients/plone-dev/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/plone-dev/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };


  fileSystems."/var/lib/containers/unibw/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/unibw/srv/unibw" = {
    device = "/home/goibhniu/Syslab/clients/unibw/container";
    options = [ "bind" ];
  };
  fileSystems."/srv/unibw" = {
    device = "/home/goibhniu/Syslab/clients/unibw/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/unibw/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };


  fileSystems."/var/lib/containers/quaivesaas/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/quaivesaas/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/quaivesaas/srv/quaivesaas" = {
    device = "/srv/quaivesaas";
    options = [ "bind" ];
  };


  fileSystems."/var/lib/containers/star-ws/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/star-ws/srv/star-ws" = {
    device = "/home/goibhniu/Syslab/clients/star-ws/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/star-ws/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };


  fileSystems."/var/lib/containers/pi/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/pi/srv/pi" = {
    device = "/home/goibhniu/Syslab/clients/pi/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/pi/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };
  fileSystems."/srv/pi" = {
    device = "/var/lib/containers/pi/srv/pi";
    options = [ "bind" ];
  };

  
  fileSystems."/var/lib/containers/recensio/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/recensio/srv/recensio" = {
    device = "/home/goibhniu/Syslab/clients/recensio/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/recensio/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };
  fileSystems."/srv/recensio" = {
    device = "/var/lib/containers/recensio/srv/recensio";
    options = [ "bind" ];
  };

  
  fileSystems."/var/lib/containers/ikath/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/ikath/srv/ikath" = {
    device = "/home/goibhniu/Syslab/clients/ikath/container";
    options = [ "bind" ];
  };
  fileSystems."/srv/ikath" = {
    device = "/var/lib/containers/ikath/srv/ikath";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/ikath/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/oshwiki/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/srv/oshwiki" = {
    device = "/var/lib/containers/oshwiki/srv/oshwiki";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/oshwiki/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };

  fileSystems."/var/lib/containers/unibw-py3/etc/nixos/goibhnix" = {
    device = "/etc/goibhnix/configurations";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/unibw-py3/srv/unibw-py3" = {
    device = "/home/goibhniu/Syslab/clients/unibw-py3/container";
    options = [ "bind" ];
  };
  fileSystems."/var/lib/containers/unibw-py3/home/goibhniu" = {
    device = "/home/goibhniu";
    options = [ "bind" ];
  };
  fileSystems."/srv/unibw-py3" = {
    device = "/var/lib/containers/unibw-py3/srv/unibw-py3";
    options = [ "bind" ];
  };

  swapDevices = [
    # { device = "/dev/sda1"; }
  ];

  hardware = {
    steam-hardware.enable = true;
    bluetooth.enable = true;
    enableAllFirmware = true;
    opengl.driSupport32Bit = true;
    pulseaudio = common.pulseaudio // audio.pulseaudio // steamPa;
    # nvidia.package = config.boot.kernelPackages.nvidiaPackages.beta;
    #sane.enable = true;
    cpu.intel.updateMicrocode = true;

    # to use bumblebee don't put nvidia in your video drivers, instead just do# hardware.bumblebee.enable = true
    # bumblebee.enable = true;
    # bumblebee.connectDisplay = true;
  };
  powerManagement = {
    enable = true;
    # Stop the disk getting parked, it reduces the lifespan
    # powerUpCommands = "${pkgs.hdparm}/sbin/hdparm -B 255 /dev/sda";
  };

  nixpkgs.config = common.config // audio.config // {
    #sane.snapscanFirmware = /firmware/esfw41.bin;
    pidgin-with-plugins = pkgs.pidgin-with-plugins.override {
      plugins = [ pkgs.pidginotr ];
    };
    firefox = {
      # enableAdobeFlash = true;
      #enableGoogleTalkPlugin = true;
      enablePlasmaBrowserIntegration = true;
    };

    chromium = {
      # enablePepperFlash = true;
    };
    kodi.enablePVRHTS = true;
  };


### software, services
  services = audio.services // {
    # Strange dns issues, can dig the domain name, and ping the result but cannot ping the domain name
    #nscd.enable = false;
    # dockerRegistry.enable = true;
    avahi.enable = true;
    avahi.nssmdns = true;
    softether.vpnclient.enable = true;
    #tlp.enable = true;
    # tlp.settings = ''
    #   CPU_SCALING_GOVERNOR_ON_AC=powersave
    #   CPU_SCALING_GOVERNOR_ON_BAT=powersave
    # '';
  #   icecast.enable = true;
  #   icecast.hostname = "0.0.0.0";
  #   icecast.admin.password = "nic3c4st";
  #   icecast.user = "goibhniu";
  #   icecast.group = "audio";
  #   icecast.extraConf = ''
  #   <mount type="normal">
  #     <mount-name>/mixxx.ogg</mount-name>
  #     <username>mixxx</username>
  #     <password>mixxx</password>
  #   </mount>
  # '';
    openvpn.servers = {
      ikath = {
        config = "${builtins.readFile /home/goibhniu/Syslab/clients/ikath/container/openvpn-config/vpn_syslab_roisteatvpn.zhkath.ch/vpn_syslab_roisteatvpn.zhkath.ch.ovpn_newer_openvpn}";
        autoStart = false;
      };
    };
#        /home/goibhniu/Syslab/clients/ikath/container/openvpn-config/vpn_syslab_roisteatvpn.zhkath.ch/vpn_syslab_roisteatvpn.zhkath.ch.ovpn

    acpid.enable = true;
    #dbus.packages = [ pkgs.gnome.GConf ];
    # postfix = {
    #   destination = [ "localhost" ]; # "eve.chaoflow.net" ];
    #   enable = true;
    #   extraConfig = ''
    #     # For all options see ``man 5 postconf``
    #     # Take care, empty lines will mess up whitespace removal. It would be
    #     # nice if empty lines would not be considered in minimal leading
    #     # whitespace analysis, but don't know about further implications. Also
    #     # take care not to mix tabs and spaces. Should tabs be treated like 8
    #     # spaces?
    #     #
    #     # ATTENTION! Will log passwords
    #     #debug_peer_level = 4
    #     #debug_peer_list = tesla.chaoflow.net
    #     inet_interfaces = loopback-only
    #     #
    #     # the nixos config option does not allow to specify a port, beware:
    #     # small 'h' in contrast to the config option with capital 'H'
    #     relayhost = [smtprelaypool.ispgateway.de]:submission
    #     #relayhost = [127.0.0.1]:1587
    #     #
    #     #XXX: needs server certificate checking
    #     #smtp_enforce_tls = yes
    #     #
    #     # postfix generic map example content:
    #     # user@local.email user@public.email
    #     # Run ``# postmap hash:/etc/nixos/cfg-private/postfix_generic_map``
    #     # after changing it.
    #     smtp_generic_maps = hash:/etc/nixos/cfg-private/postfix_generic_map
    #     smtp_sasl_auth_enable = yes
    #     smtp_sasl_mechanism_filter = plain, login
    #     #
    #     # username and password for smtp auth, example content:
    #     # <relayhost> <username>:<password>
    #     # The <relayhost> is exactly what you specified for relayHost, resp.
    #     # relayhost.
    #     smtp_sasl_password_maps = hash:/etc/nixos/cfg-private/postfix_passwd
    #     smtp_sasl_security_options = noanonymous
    #     smtp_sasl_tls_security_options = $smtp_sasl_security_options
    #     smtp_use_tls = yes
    #   '';
    #   hostname = "eve.chaoflow.net";
    #   origin = "eve.chaoflow.net";
    #   postmasterAlias = "root";
    #   rootAlias = "cfl";
    # };
    locate.enable = true;
    #locate.interval = "53 10 * * *";
    # Rad1o
    # udev.extraRules = ''
    #   SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="6089", MODE:="0666"
    #   SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="cc15", MODE:="0666"
    # '';
    udev.extraRules = ''
      ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="6028" RUN+="${pkgs.bash}/bin/sh -c 'echo enabled > /sys/bus/usb/devices/usb1/power/wakeup'"
      KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0666", TAG+="uaccess", TAG+="udev-acl"
    '';

    mysql = {
      enable = true;
      package = pkgs.mysql;
    };
    # openvpn = {
    #   enable = true;
    #   servers = {
    #     ikath = {
    #       config = "${builtins.readFile /home/goibhniu/Syslab/clients/ikath/openvpn-config/vpn_syslab_roisteatvpn.zhkath.ch/vpn_syslab_roisteatvpn.zhkath.ch.ovpn}";
    #       autoStart = false;
    #     };
    #   };
    # };

    journald.extraConfig = "SystemMaxUse=256M";

    nginx = {
        enable = false;
        virtualHosts = {
          "k2p0vrjzprfhfubn.myfritz.net" = {
            locations."/" = {
              root = "/var/www";
            };
          };
        };

        # #config = ''
        # worker_processes 2;
        # events {
        #    worker_connections 1024;
        # }

        # http {
        #    sendfile on;
        #    tcp_nopush on;
        #    tcp_nodelay on;
        #    keepalive_timeout 65;
        #    types_hash_max_size 2048;
        #    client_max_body_size 64M;

        #    server_names_hash_bucket_size 64;

        #    include ${pkgs.nginx}/conf/mime.types;
        #    default_type application/octet-stream;

        #    access_log /var/log/nginx_access.log;
        #    error_log /var/log/nginx_error.log;

        #    gzip off;
        #    gzip_disable "msie6";

        #    server {
        #        listen *:80;
        #        server_name dpd;

        #        # Enable gzip compression
        #        gzip             off;
        #        gzip_min_length  1000;
        #        gzip_proxied     any;
        #        gzip_types       text/xml text/plain text/kss application/xml text/css application/x-javascript;

        #        location /past_defects {
        #            proxy_pass http://127.0.0.1:6542;
        #            proxy_connect_timeout 75;
        #            proxy_read_timeout 185;
        #            proxy_redirect      off;
        #            proxy_set_header        Host $host;
        #            proxy_set_header        X-Real-IP $remote_addr;
        #            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        #            proxy_redirect  default;
        #        }

        #        location /static {
        #            proxy_pass http://127.0.0.1:6540;
        #        }
        #        location / {
        #            rewrite ^/(.*)$ /VirtualHostBase/http/dpd:80/Plone/VirtualHostRoot/$1 break;
        #            proxy_pass http://127.0.0.1:6541;

        #            proxy_connect_timeout 75;
        #            proxy_read_timeout 185;
        #            proxy_redirect      off;
        #            proxy_set_header        Host $host;
        #                proxy_set_header        X-Real-IP $remote_addr;
        #            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        #            proxy_redirect  default;
        #        }
        #    }
        # }
        # '';
    };
    # nfs.server = {
    #   enable = true;
    #   exports = ''
    #     /mnt/bulk/databases 10.0.2.0/24(rw,sync,all_squash,anonuid=1000,anongid=100,subtree_check)
    #     /srv/nfs/eggs 10.0.2.0/24(rw,sync,all_squash,anonuid=1000,anongid=100,subtree_check)
    #   '';
    # };
    # openldap.enable = true;
    openssh = {enable = true; ports = [ 2222 ];};
    printing = {
      drivers = [ pkgs.gutenprint ];
      enable = true;
    };
    xserver = {
      desktopManager.plasma5.enable = true;
      displayManager = {sddm.enable = true; defaultSession = "plasma";};
      videoDrivers = ["intel" "nvidia"];
      enable = true;
      layout = "gb";
      libinput.enable = true;
      wacom.enable = true;
      # multitouch.enable = true;
      # monitorSection = ''
      #     HorizSync       53.3 - 66.7
      #     VertRefresh     48.0 - 60.0
      #   Option         "DPMS"
      #   Modeline "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
      # '';
      # screenSection = ''
      #   #Monitor        "Monitor[0]"
      #   DefaultDepth    24
      #   #Option         "Stereo" "0"
      #   #Option         "nvidiaXineramaInfoOrder" "DFP-1"
      #   #Option         "metamodes" "1920x1080_60.00"
      #   #Option         "SLI" "Off"
      #   #Option         "MultiGPU" "Off"
      #   #Option         "BaseMosaic" "off"
      #   SubSection     "Display"
      #       Depth       24
      #       Modes       "1920x1080_60.00" "1920x1080"
      #   EndSubSection
      #   # Option "UseEDID" "False"
      #   Option "UseEDIDFreqs" "False"
      #   #Option "ModeValidation" "NoEdidModes, NoXServerModes, NoVesaModes, AllowNonEdidModes"
      #   Option "ModeValidation" "NoXServerModes, NoVesaModes, AllowNonEdidModes"
      # '';
      # deviceSection = ''
      #    Option "ModeValidation" "NoEdidModes"
      # '';
     };
  };

  environment = {
    systemPackages = with packageGroups;
     desktop ++ desktop64 ++ dev ++ internet ++ multimediaConsumption ++
     networking ++ text ++ utilsFull ++ multimediaProduction ++ [(pkgs.kodi.passthru.withPackages(kodiPkgs: with kodiPkgs; [ pvr-hts youtube steam-controller ]))] ++
     [
       pkgs.ansible
       pkgs.arduino
       pkgs.chromedriver
       pkgs.esptool-ck
       pkgs.feh
       pkgs.ffmpeg
       pkgs.jamulus
       pkgs.i2c-tools
       pkgs.libreoffice
       pkgs.minicom
       pkgs.mosquitto
       pkgs.peek
       pkgs.pre-commit
       pkgs.signal-desktop
       pkgs.sshuttle
       pkgs.yarn
       pkgs.cpufrequtils
       pkgs.geckodriver
       pkgs.mattermost-desktop
       pkgs.obs-studio
       pkgs.pwgen
       pkgs.python3Packages.flake8
       pkgs.python3Packages.black
       pkgs.qt5.qttools
     ];   #pkgs.blender pkgs.steam  blenderCuda pkgs.audacious  ffmpegNvenc
    shellAliases = common.shellAliases;
    pathsToLink = [ "/${pkgs.gimp.name}-plugins" ];
    shellInit = audio.shellInit;
  };

  programs.bash = common.bash;
  programs.gnupg.agent.enable = true;
  programs.gnupg.agent.enableSSHSupport = true;
  programs.adb.enable = true;
  programs.dconf.enable = true;

  fonts.fontconfig.subpixel.rgba = "bgr";
  console.keyMap = "uk";
  i18n.defaultLocale = "en_GB.UTF-8";
  networking = {
    networkmanager.unmanaged = [
      "interface-name:ve-*"
    ];
    firewall.allowedTCPPorts = [ 21 22 80 443 5353 8000 8080 3819 2021 ];
    firewall.allowedUDPPorts = [ 3819 2021 ];
    firewall.allowedTCPPortRanges = [
      { from = 1714; to = 1764; } # kdeconnect
      { from = 9101; to = 9103; } # bacula
    ];
    firewall.allowedUDPPortRanges = [
      { from = 1714; to = 1764; }
    ];
    enableIPv6 = true;
    networkmanager.enable = true;
    hostName = "work";
    extraHosts = ''
      127.0.0.2 dpd
      46.4.69.15 oshwikismw
      192.168.1.50 monster
      192.168.1.51 android
      46.4.69.144 oshwiksmw.syslab.com
      46.4.69.144 oldoshwiki
      127.0.0.15 osha.goibh.eu dummy.goibh.eu
      127.0.0.16 star.goibh.eu
      10.0.2.100 eithniu
      10.0.2.100 xstar.eithniu.eu xstarbot.eithniu.eu
      10.233.8.2 star.eithniu.eu starbot.eithniu.eu test.star.eithniu.eu hottopics.eithniu.eu
      10.0.2.101 osha.eithniu.eu
      10.0.2.102 bx.eithniu.eu
      10.0.2.103 recensio.eithniu.eu
      10.0.2.104 densoqs.eithniu.eu
      192.168.2.180 shrek
      144.76.32.116 osha.goibhniu.ext9.eu
      10.233.1.2 csr
      10.233.12.2 hw2014
      10.233.3.2 gfb
      10.233.4.2 philrom
      10.233.5.2 oiraproject client.oiraproject.eithniu.eu admin.oiraproject.eithniu.eu
      10.233.6.2 brandbook
      10.233.7.2 pdfdiff
      10.233.8.2 star
      10.233.9.2 eca
      10.233.10.2 plone-dev
      10.233.11.2 star-ws
      127.0.0.1 oshawiki01.gocept.net
      10.233.13.2 pi
      10.233.14.2 ikath.eithniu.eu
      10.233.15.2 tec
      10.233.16.2 unibw
      10.233.18.2 quaivesaas
      10.233.20.2 oshwiki
      10.233.22.2 recensio-container
      194.30.98.123 staging-oshwiki

      #178.79.171.36 www.brightsideonlinementoring.co.uk
      178.79.170.58 archive.brightsidecloud.org
      178.79.170.58 soas
      #10.172.40.19 pentest.bayern-gegen-linksextremismus.bayern.de
      #10.172.40.19 rvr-prx.bayern.de
      137.193.6.62 beta-cms5.rz.unibw-muenchen.de
      137.193.6.62 staging-cms5.rz.unibw-muenchen.de
      137.193.6.62 testing-cms5.rz.unibw-muenchen.de
      137.193.6.37 webfrontend webfrontend.rz.unibw-muenchen.de nginxtest.rz.unibw-muenchen.de
      192.168.20.109 webfrontend-192
      192.168.20.30 zeo
      192.168.20.39 zodbrelaunch memcachedserver
      10.19.0.130 zeo-Filer
      10.19.0.139 zodbrelaunch-Filer
      10.233.21.2 unibw-py3
      178.79.171.36  staging-ementoring.brightsidecloud.org

      185.146.65.28 extranet.osha.europa.eu oshanet.osha.europa.eu
      #88.99.167.145 www.imu-institut.de

      195.62.126.75 www.recensio.net www.rezensio.net rezensio.net recensio.net recensio.de www.recensio.de www.recensio-regio.net www.rezensio-regio.net www.recensio-regio.de www.rezensio-regio.de recensio-regio.net rezensio-regio.net recensio-regio.de rezensio-regio.de


      192.168.1.208 scamall.cillian.org
      '' + "${builtins.readFile /home/goibhniu/Syslab/clients/ikath/openvpn-config/vpn_syslab_roisteatvpn.zhkath.ch/hosts}";

    nat = {
      enable = true;
      internalInterfaces = ["ve-+"];
      externalInterface =  "enp2s0f1"; #"wlp3s0";  "enp0s20f0u3";
    };
  };
  time.timeZone = "Europe/Dublin";

  users.extraUsers.goibhniu = {
    uid = 1000;
    home = "/home/goibhniu";
    shell = "/run/current-system/sw/bin/bash";
    extraGroups = [ "wheel" "audio" "adbusers"];
  };
  users.users.goibhniu.isNormalUser = true;

  fonts.fonts = common.fonts;

  security.pam.loginLimits = audio.loginLimits;

  # virtualisation.virtualbox.host = {
  #   enable = true;
  #   enableExtensionPack = true;
  # };

  
  system.stateVersion = "18.09";

  nix.extraOptions = "binary-caches-parallel-connections = 10";
  nix.settings.trusted-public-keys = [ "hydra.nixos.org-1:CNHJZBh9K4tP3EKF6FkkgeVYsS3ohTl+oS0Qa8bezVs="  "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];
  nix.settings.sandbox = true;
  # nix.settings.substituters = [ "https://nixcache.reflex-frp.org" ];
}

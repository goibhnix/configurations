{pkgs, ...}: with pkgs;
rec {

  erlang_wx = erlang.override { wxSupport = true; };

  xsaneGimp = xsane.override { gimpSupport = true; };

  ffmpegNvenc = ffmpeg-full.override { nvenc = true;  nonfreeLicensing = true; };

  blenderCuda = blender.override { cudaSupport = true; };

  vcs = [ gitAndTools.gitFull gitAndTools.pre-commit ];

  dev = vcs ++ [ emacs gettext sshuttle xmlstarlet ] ++ javaScript ++ [ erlang ] ++ nix;   # ++ stdenv.lib.optionals (stdenv.is64bit) [ chromedriver ];

  nix = [ nixpkgs-lint ];

  goibhnix = [ git xfsprogs ];

  javaScript = [ nodejs ]; #nodePackages.jshgint

  python = pkgs.python27Full.buildEnv.override {
    extraLibs = with pkgs.python27Packages; [
      epc ipython jedi lxml markdown pylint pymacs readline rope
      ropemacs ropemode sqlite3 virtualenv
    ];
  };

  desktop = [
    ark ksshaskpass pinentry-qt anki feh kdeconnect pmutils
    pavucontrol plasma-browser-integration
    yt-dlp
  ]; #libreoffice adobe-reader
  desktop64 = [ signal-desktop ];
  internet = [ chromium firefox thunderbird ];

  audioProduction = [
    a2jmidid ams-lv2 ardour asunder audacity bristol calf drumkv1
    hydrogen jalv jack_capture jack2Full ladspaPlugins lilv mhwaveedit
    mda_lv2 puredata qjackctl qsampler qtractor rakarrack rubberband
    samplv1 seq24 sox swh_lv2 synthv1
    musescore 
  ]; #drumgizmo petrifoo seq24 ingen linuxsampler qsynth  gigedit distrho yoshimi  pianobooster sonic-visualiser setbfree 

  videoProduction = [ kdenlive ];

  gimpExtras = with gimpPlugins; [
    fourier lightning lqrPlugin  waveletSharpen
  ]; #gap gmic focusblur texturize   gimplensfun ufraw resynthesizer

  graphicsProduction = gimpExtras ++ [
    gimp gutenprint imagemagick inkscape
  ]; #saneFrontends saneBackends xsaneGimp

  screencasting = [  keymon xorg.xkbcomp mesa xkeyboard_config ];

  multimediaProduction = builtins.concatLists [
    audioProduction videoProduction graphicsProduction
  ];

  multimediaConsumption = [ clementine vlc vorbis-tools ];

  multimediaFull = multimediaProduction ++ multimediaConsumption;
  # xf87_input_wacom];

  networking = [ bind bridge-utils dhcpcd dnsmasq iptables tunctl ];

  text = [ aspell aspellDicts.en aspellDicts.ga corefonts emacs mupdf xorg.xmodmap ];

  emacs = (import ./emacs.nix { inherit pkgs; });
  
  serverUtils = [ screen wget curl lsof openssl file which ];

  compressionUtils = [ unzip xz zip ];

  systemUtils = [
    cryptsetup dbus.out gnumake qemu_kvm openvpn vde2
  ];

  userUtils = [  ]; # wine

  utilsFull = builtins.concatLists [
    serverUtils
    compressionUtils
    systemUtils
    userUtils
  ];

  zotonic = [ erlang gcc gnumake imagemagickBig ];
}

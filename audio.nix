{pkgs, ...}: with pkgs;

{
  postBootCommands = "${pkgs.procps}/sbin/sysctl -w vm.swappiness=10";

  pulseaudio = {package = pkgs.pulseaudioFull.override { jackaudioSupport = true;};
  };

  config = {
    mumble.jackSupport = true;
    pulseaudio = true;
  };

  services = {
    udev = {
      # packages = [ pkgs.ffadoFull ];
      extraRules = ''
        KERNEL=="rtc0", GROUP="audio"
        KERNEL=="hpet", GROUP="audio"
      '';
    };
  };

  loginLimits = [
    { domain = "@audio"; item = "memlock"; type = "-"; value = "unlimited"; }
    { domain = "@audio"; item = "rtprio"; type = "-"; value = "99"; }
    { domain = "@audio"; item = "nofile"; type = "soft"; value = "99999"; }
    { domain = "@audio"; item = "nofile"; type = "hard"; value = "99999"; }
    { domain = "@pulse-rt"; item = "rtprio"; type = "-"; value = "9"; }
    { domain = "@pulse-rt"; item = "nice"; type = "-"; value = "-11"; }
  ];

  powerManagement = { cpuFreqGovernor = "performance"; };

  shellInit = ''
    export LV2_PATH=/nix/var/nix/profiles/default/lib/lv2:/var/run/current-system/sw/lib/lv2
  '';
}

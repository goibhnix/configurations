{ pkgs ? import <nixpkgs> {} }:

let
  myEmacs = pkgs.emacs;
  emacsWithPackages = (pkgs.emacsPackagesFor myEmacs).emacsWithPackages;
in
  emacsWithPackages (epkgs: (with epkgs.melpaStablePackages; [
    magit          # ; Integrate git <C-x g>
#       undo-tree      # ; <C-x u> to show the undo tree
    ace-jump-mode
# #      ack-and-a-half
       auto-complete
# #      colorThemeSolarized
#       # company
#       erlangMode
       expand-region
       fastnav
       flycheck
# #      git-timemachine
#     json
      haskell-mode
#       # helm
      markdown-mode
      multiple-cursors
       nix-mode
#       # org-plus-contrib
       paredit
       rainbow-delimiters
       use-package
       # yasnippet
       ###########       flymake-cursor
#       # volatile-highlights
       #rainbow-mode
       #yasnippet-bundle
  ]) ++ (with epkgs.melpaPackages; [



  ]) ++ (with epkgs.elpaPackages; [
       ]) ++ [


  ])
  

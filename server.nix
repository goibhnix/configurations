{pkgs, ...}: with pkgs;
{
  services = {
    locate.enable = true;
    openssh = {enable = true; ports = [ 2222 ];};
    postgresql = {
        enable = true;
        package = pkgs.postgresql92;
    };
  };

  misc = {
    i18n = {consoleKeyMap = "uk"; defaultLocale = "en_GB.UTF-8";};
    time.timeZone = "Europe/Berlin";
  };

  shellInit = ''
    gpg-connect-agent /bye
    GPG_TTY=$(tty)
    export GPG_TTY
    unset SSH_AGENT_PID
    export SSH_AUTH_SOCK="''${HOME}/.gnupg/S.gpg-agent.ssh"
    export LIBRARY_PATH=/var/run/current-system/sw/lib
    export C_INCLUDE_PATH=/var/run/current-system/sw/include
  '';

}
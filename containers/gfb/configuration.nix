{ config, pkgs, ... }:

with pkgs.lib;
let
  options = import ../options.nix {inherit pkgs;};
  name = "gfb";
in

{ imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  users.extraUsers.gfb = options.userBase // { home = "/srv/${name}"; };

  environment = {
    systemPackages = with pkgs; [
      openldap openjdk
    ] ++ options.plonePackages;

    shellInit = ''
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.openldap}/include:${pkgs.cyrus_sasl}/include/sasl
    '';

  };
}

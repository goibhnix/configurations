{ config, pkgs, ... }:
with pkgs.lib;

{ boot.isContainer = true;
  security.initialRootPassword = mkDefault "!";
  networking = {
    hostName = mkDefault "mw";
    useDHCP = false;
    firewall.allowedTCPPorts = [ 80 22 ];
  };
  services = {
    # ejabberd.enable = true;
    openssh.enable = true;
    # solr = {
    #   enable = true;
    #   user = "jochi";
    #   group = "solr";
    #   solrHome = "/srv/solr";
    # };
    postgresql = {
      enable = true;
      package = pkgs.postgresql92;

    authentication = mkOverride 10 ''
      local mediawiki all ident map=mwusers
      local all all ident
    '';
    identMap = ''
      mwusers root mediawiki
      mwusers wwwrun mediawiki
    '';
    };
    mysql = {
      enable = true;
      package = pkgs.mysql55;
    };
  };

  
  services.httpd = {
    enable = true;
    adminAddr = "admin@example.org";
    virtualHosts = [
      { 
       hostName = "wiki";
       extraConfig = ''
         RedirectMatch ^/$ /mywiki
       '';
       extraSubservices = [
         { serviceType = "mediawiki";
           siteName = "My Wiki";
           articleUrlPrefix = "/mywiki";
           logo = "http://www.example.org/wiki-logo.png"; # should be 135x135px
           extraConfig =
             ''
               # See http://www.mediawiki.org/wiki/Manual:Configuration_settings
               $wgEmailConfirmToEdit = true;
             '';
         }
       ];
     }
   ];
  };

  users = {
    mutableUsers = true;
    extraUsers.jochi = {
      createHome = true;
      home = "/srv/jochi";
      description = "Dev Account";
      extraGroups = [ "wheel" "solr" ];
      uid = 1001;
      useDefaultShell = true;
      openssh.authorizedKeys.keys = [ 
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwjS/Y5IlDmJm9TmIz4//oPJpdrdkIEkJek2rdqw9fMPKnnjUOOP/G4QCdcPySfofFiwpTQ1Q80v6rXEFD71HgsFfP+WyCTB8cXXZWiPVqIeLtZxpkdJ947W+qsUD6ato9hFogG9YfBxLGffv6rz3v8mOPDisG3EkCSNy8pPX2j4r7vplcHP86vq5wzph3XhcGYt3y+JqGkO7RcnhZ3lC7mzfC5YLz2oD+Sp2egMdOPcsD/7FkYMyW8V6rf4ISPITOZ/i+B654lQN2FRMPIO+nqeqDKegQhsXJiWLxdnuqRQjkkXks8qNKzTxNpbUvQU8I/CT1sOEvE4dzNY9S4aTf goibhniu@goibhniu"
      ];
    };
  };
  users.extraGroups.solr.gid = 5000;

  environment = {
    systemPackages = with pkgs; [
      git
      lsof
    ];
  };
}

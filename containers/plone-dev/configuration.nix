{ config, pkgs, ... }:

with pkgs.lib;

let
  options = import ../options.nix {inherit pkgs;};
  server = import ../../server.nix {inherit pkgs;};
  name = "plone-dev";
  #dpython3 = pkgs.enableDebugging pkgs.python3;
in
{
  imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  services.redis.enable = true;

  users.extraUsers.plone-dev = options.userBase // { home = "/srv/plone-dev"; };

  environment = {
    systemPackages = with pkgs; [
      gnumake gdb nodejs ruby enchant libffi.dev openssl.dev openssl.out
      file imagemagick graphicsmagick poppler_utils
      ghostscript pdftk libreoffice openjdk wkhtmltopdf gnupg
      openldap
    ] ++ options.plone3Packages;
    pathsToLink = [ "/include" ];
    shellInit =  ''
      export LD_LIBRARY_PATH=${pkgs.enchant}/lib:${pkgs.openssl.out}/lib
      export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant}/include:${pkgs.zlib}/include:${pkgs.openldap.dev}/include:${pkgs.cyrus_sasl.dev}/include/sasl
      export DIAZO_ALWAYS_CACHE_RULES=1
    ''; #server.shellInit +
  };

  #nix.useSandbox = false;

}

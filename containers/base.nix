{ config, pkgs, ... }:

with pkgs.lib;

let
  common = import ../common.nix {inherit pkgs;};
in
{ boot.isContainer = true;
  security.initialRootPassword = mkDefault "!";
  networking = {
    useDHCP = false;
    firewall.enable = false;
  };
  services.openssh.enable = true; 
  programs.bash = common.bash;
  environment = {
    pathsToLink = [ "/include" ];

    # extend with config.environment.shellInit
    shellInit = ''
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include
      export CPLUS_INCLUDE_PATH=/var/run/current-system/sw/include
    '';
  };

}


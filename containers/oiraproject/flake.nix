{
  inputs.extra-container.url = "github:erikarvstedt/extra-container";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";

  outputs = { extra-container, ... }@inputs:
    extra-container.inputs.flake-utils.lib.eachSystem extra-container.lib.supportedSystems (system: {
      packages.default = extra-container.lib.buildContainers {
        # The system of the container host
        inherit system;

        # Optional: Set nixpkgs.
        # If unset, the nixpkgs input of extra-container flake is used
        nixpkgs = inputs.nixpkgs;

        # Set this to disable `nix run` support
        # addRunner = false;

        config = {
          containers.oiraproject = {
            privateNetwork = true;
            hostAddress = "10.250.0.1";
            localAddress = "10.233.5.2";
            bindMounts = {
              "/srv" = {
                hostPath = "/srv";
                isReadOnly = false;
              };
              "/home/goibhniu" = {
                hostPath = "/home/goibhniu";
                isReadOnly = false;
              };
            };
            extra = {
              enableWAN = true;
              exposeLocalhost = true;
              firewallAllowHost = true;
            };

            config = { pkgs, ... }:
            let
              options = import ../options.nix {inherit pkgs;};
              name = "oiraproject";
            in
            {
              users.extraUsers.oiraproject = options.userBase // {
                home = "/srv/${name}";
              };
              services = {
                cron.mailto = "";
                postgresql = {
                  enable = true;
                  # package = pkgs.postgresql92;
                  authentication = ''
                    local all all ident
                    host all all 10.233.5.2/32 trust
                  '';
                  enableTCPIP = true;
                };
                nginx = {
                  enable = true;
                  config = ''
                    worker_processes 2;
                    events {
                    worker_connections 1024;
                    }

                    http {
                    include /srv/oiraproject/oira.batou/work/_/etc/nginx/local-modified/admin.oiraproject.eithniu.eu.conf;

                    include /srv/oiraproject/oira.batou/work/_/etc/nginx/local-modified/client.oiraproject.eithniu.eu.conf;
                    }
                  '';
                };
                openssh.enable = true;
              };
              nixpkgs.config.permittedInsecurePackages = [
                "qtwebkit-5.212.0-alpha4"
              ];
              environment = {
                systemPackages = with pkgs; [
                  ruby bundler gnumake gcc nodejs libiconv unzip sqlite enchant wkhtmltopdf
                  openldap
                ] ++ options.plone3Packages;
                pathsToLink = [ "/include" ];
                shellInit = ''
                  export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant.dev}/include:${pkgs.zlib}/include:${pkgs.openldap.dev}/include:${pkgs.cyrus_sasl.dev}/include/sasl
                  export LD_LIBRARY_PATH=${pkgs.enchant}/lib
                  export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
                '';
              };
            };
          };
        };
      };
    });
}

{ config, pkgs, ... }:

with pkgs.lib;

let
  options = import ../options.nix {inherit pkgs;};
  name = "oiraproject";
in
{
  imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  networking.extraHosts = ''
    10.233.5.2 oiraproject.eithniu.eu
    127.0.0.1 dummy.eithniu.eu localhost.eithniu.eu
  '';

  services = {
    memcached.enable = true;
    cron.mailto = "";
    postgresql = {
      enable = true;
      package = pkgs.postgresql92;
      authentication = ''
        local all all ident
        host all all 10.233.5.2/32 trust
      '';
      enableTCPIP = true;
    };
    nginx = {
      enable = true;
      config = ''
        worker_processes 2;
        events {
         worker_connections 1024;
        }

        http {
          include /srv/oiraproject/oira.batou/work/_/etc/nginx/local-modified/admin.oiraproject.eithniu.eu.conf;

          include /srv/oiraproject/oira.batou/work/_/etc/nginx/local-modified/client.oiraproject.eithniu.eu.conf;
        }
      '';
    };
  };

  users.extraUsers.oiraproject = options.userBase // { home = "/srv/${name}"; };
  
  environment = {
    systemPackages = with pkgs; [
      ruby bundler gnumake gcc nodejs libiconv subversion unzip sqlite mercurial enchant
    ] ++ options.plonePackages;
    shellInit = ''
      export C_INCLUDE_PATH=/var/run/current-system/sw/include
      export LD_LIBRARY_PATH=${pkgs.enchant}/lib
      export LIBRARY_PATH=${pkgs.enchant}/lib
    '';
  };
}

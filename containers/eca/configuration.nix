{ config, pkgs, ... }:

with pkgs.lib;

let
  options = import ../options.nix {inherit pkgs;};
in
{
  imports = [ ../base.nix ];
  
  networking.hostName = mkDefault "eca";

  users.extraUsers.eca = options.userBase // { home = "/srv/eca"; };

  environment = {
    systemPackages = with pkgs; [
        graphviz openldap pkgconfig
    ] ++ options.plonePackages;
    shellInit = ''
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.openldap}/include:${pkgs.cyrus_sasl}/include/sasl
    '';
  };

  services = {
    openldap = {
      enable = true;
      extraConfig = ''
        include    ${pkgs.openldap}/etc/openldap/schema/core.schema
        include    ${pkgs.openldap}/etc/openldap/schema/corba.schema
        include    ${pkgs.openldap}/etc/openldap/schema/cosine.schema
        include    ${pkgs.openldap}/etc/openldap/schema/duaconf.schema
        include    ${pkgs.openldap}/etc/openldap/schema/dyngroup.schema
        include    ${pkgs.openldap}/etc/openldap/schema/inetorgperson.schema
        include    ${pkgs.openldap}/etc/openldap/schema/java.schema
        include    ${pkgs.openldap}/etc/openldap/schema/misc.schema
        include    ${pkgs.openldap}/etc/openldap/schema/nis.schema
        include    ${pkgs.openldap}/etc/openldap/schema/openldap.schema
        include    ${pkgs.openldap}/etc/openldap/schema/ppolicy.schema
        include    ${pkgs.openldap}/etc/openldap/schema/collective.schema
        include    /openldap/eca.schema
        
        allow bind_v2
        
        #   # Do not enable referrals until AFTER you have a working directory
        #   # service AND an understanding of referrals.
        #   #referral   ldap://root.openldap.org
        
        #   pidfile             pid
        #   argsfile        args

        '' + builtins.readFile ./openldap.secrets +
        "directory   /openldap/openldap-eca-data";
      };
  };
}

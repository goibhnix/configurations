{ config, pkgs, ... }:

with pkgs.lib;

let
  options = import ../options.nix {inherit pkgs;};
in
{
  imports = [ ../base.nix ];

  networking = {
    hostName = mkDefault "star-ws";
    extraHosts = ''
      127.0.0.1 dummy
    '';
  };

  users.extraUsers.star-ws = options.userBase // { home = "/srv/star-ws"; };

  environment = {
    systemPackages = with pkgs; [
      cyrus_sasl openldap nodejs ruby 
    ] ++ options.plonePackages;
    # rubyLibs.highline rubyLibs.sass
    #   rubyLibs.blankslate rubyLibs.jekyll rubyLibs.bundler rubyLibs.thor
    shellInit = ''
      # help pip to find libz.so when building lxml
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      # ditto for header files, e.g. sqlite
      # cyrus_sasl is required by python ldap, and needs an extra subdir added
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.cyrus_sasl}/include/sasl
    '';
  };

  services = {
    varnish = {
      enable = true;
      config = builtins.readFile /srv/star-ws/build/whiteside.batou/work/_/etc/varnish/default.vcl;
    };
    nginx = {
      enable = true;
      config = ''
        worker_processes 2;
        events {
         worker_connections 1024;
        }

        http {
          include /srv/star-ws/build/whiteside.batou/work/_/etc/nginx/local/whiteside.conf;
        }
      '';
    };
  };

}

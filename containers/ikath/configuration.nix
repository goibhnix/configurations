{ config, pkgs, ... }:

with pkgs.lib;
let
  options = import ../options.nix {inherit pkgs;};
  name = "ikath";
in

{ imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  networking.extraHosts = ''
      10.233.14.2 ikath.eithniu.eu
      10.233.14.2 dummy.eithniu.eu
      10.100.181.11 staging.zhkath.ch
  '' + "${builtins.readFile /srv/ikath/openvpn-config/vpn_syslab_roisteatvpn.zhkath.ch/hosts}";
  # networking.nameservers = [ "8.8.8.8" ];
  # networking.interfaces = {
  #   tun0 = {
  #     virtual = true;
  #     virtualType = "tun";
  #   };
  # };

  # name = "${name}";
  users.extraUsers.ikath = options.userBase // { home = "/srv/${name}"; };

  services.redis.enable = true;
  services.openvpn.servers = {
    ikath = {
      config = "${builtins.readFile /srv/ikath/openvpn-config/vpn_syslab_roiste_at_vpn.zhkath.ch.ovpn}";
      autoStart = false;
    };
  };


  environment = {
    systemPackages = with pkgs; [
      cyrus_sasl gnumake nodejs ruby enchant sqlite
      libffi.dev python27Packages.docutils libyaml openldap openldap.dev file graphicsmagick poppler ghostscript pdftk libreoffice elinks gnupg
      mercurial openjdk subversion pcre.dev poppler_utils unzip mysql.out mysql.dev mysql openssl.dev openssl.out libedit ncurses.dev
    ] ++ options.plonePackages;
    # ln -sf /run/current-system/sw/lib/libreoffice /usr/lib/
    # tests fail because collective.documentviewer calls soffice like this:
    # SYSUSERCONFIG=file:///srv/ikath/build/ttt/tmp /usr/lib/libreoffice/program/soffice --headless --invisible  --norestore --nolockcheck --convert-to pdf --outdir ./tmp/ /tmp/tmpsVNd4K/test-file/dump.dat
    # which creates tmp/libreofficedev, but collective.documentviewer
    # expects there to be tmp/libreoffice, and removes that instead, could be a nix thing
    # see: .gem/ruby/2.1.0/gems/docsplit-0.7.6/lib/docsplit/pdf_extractor.rb
    # editing collective/documentviewer/convert.py and removing
    # libreofficedev instead of libreoffice "fixes" this
    
    # gem install --user-install bundle
    # bundle install --path /srv/ikath/.gem --binstubs
    # bundle exec jekyll serve --baseurl "" --host 0.0.0.0
    shellInit = ''
      export LD_LIBRARY_PATH=${pkgs.enchant}/lib:${pkgs.openssl.out}/lib
      export PLONE_CSRF_DISABLED=true
      export PATH=$HOME/.gem/ruby/2.1.0/bin:$PATH
      export ZSERVER_HOST=10.233.14.2
      export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant}/include:${pkgs.cyrus_sasl.dev}/include/sasl:/var/run/current-system/sw/include/mysql/server
    '';

  };
}

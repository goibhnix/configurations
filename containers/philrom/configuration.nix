{ config, pkgs, ... }:

with pkgs.lib;

let
  options = import ../options.nix {inherit pkgs;};
in
{
  imports = [ ../base.nix ];

  networking.hostName = mkDefault "philrom";

  users.extraUsers.philrom = options.userBase // { home = "/srv/philrom"; };

  environment = {
    systemPackages = with pkgs; [
      abiword
      jdk
      mercurial
      ncurses # for readline, probably not being used
      openldap
    ] ++ options.plonePackages;

    shellInit = ''
      # help pip to find libz.so when building lxml
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      # ditto for header files, e.g. sqlite
      # cyrus_sasl is required by python ldap, and needs an extra subdir added
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.cyrus_sasl}/include/sasl:${pkgs.readline}/include/readline
    '';
  };
}

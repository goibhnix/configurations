{ config, pkgs, ... }:

with pkgs.lib;
let
  options = import ../../options.nix {inherit pkgs;};
  name = "osha";
in
{
  imports = [ ../../base.nix ];
  networking.hostName = mkDefault "hw2014";
  networking.extraHosts = ''
      10.233.12.2 hw2014
    '';

  services = {
    nginx.enable = true;
  };

  users.extraUsers.osha = options.userBase // { home = "/srv/${name}"; };

  environment = {
    systemPackages = options.plonePackages;
    pathsToLink = [ "/include" ];
  };
}

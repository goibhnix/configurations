{ config, lib, pkgs, ... }:

with lib;

{ boot.isContainer = true;
  security.initialRootPassword = mkDefault "!";
  networking = {
    hostName = mkDefault "screencast";
    useDHCP = false;
    firewall.enable = false;
  };

  services = {
    openssh.enable = true;
    postgresql = {
      enable = true;
      package = pkgs.postgresql92;
      authentication = pkgs.lib.mkOverride 10 ''
        local mediawiki all ident map=mwusers
        local all all ident
      '';
      identMap = ''
        mwusers root mediawiki
        mwusers wwwrun mediawiki
      '';
    };

    httpd = {
      enable = true;
      adminAddr = "blah@example.com";
      virtualHosts = [
        { hostName = "screenhome";
          documentRoot = "/home/mw";
        }
        { hostName = "screenwiki";
          extraConfig = "RedirectMatch ^/$ /mywiki";
          extraSubservices = [
            { serviceType = "mediawiki";
              siteName = "Screen Wiki";
              articleUrlPrefix = "/mywiki";
              extraConfig = ''
                $wgEmailConfirmToEdit = true;  
              '';
            }
          ];
        }
      ];
    };
  };  

  users = {
    mutableUsers = true;
    extraUsers.mw = {
      createHome = true;
      home = "/home/mw";
      description = "Dev account";
      extraGroups = [ "wheel" ];
      uid = 1001;
      useDefaultShell = true;
      openssh.authorizedKeys.keys = [ 
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKvsA2hFOGSx+kys6U7NxKZVRlmbMhUkp3ceJTPaqR/L3u0RsZJragktGUVVt7cdNg1QYRCE4VTJv4z3G2Pv+ZLuCV/HDBHAq63JfcKF7IzQkZfW77Ac+ulrWmbC6TBfYIGcppJ+XMdkn/sF3X1XiVheFdKY2VMdjmvHZjkhuYD8XQFM5EwYq2bSluZ0rhrzIwOO4ZTBa54/wGP1YDzOdJe60XLwj0zcUfxnCa0TrHoQFGcL6I82gXrKBEy3bOqfC1yB0BtgDXyDLBSv/BTVH1nOtD/Ur2ufgP7H7pcO5hbKsr4+Xk9upjrIxhdx3TA5qTwF4XnrawI9vIhsZbUl13 goibhniu@goibhniu"
      ];
    };
  };

}

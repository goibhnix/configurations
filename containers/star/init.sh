#!/run/current-system/sw/bin/bash

# Since we do want to have access to the system python modules, let's
# create the virtualenvs without --no-site-packages
# We also need to trick py2.6 into finding _ssl.so

BATOU_USER=`whoami`
sudo mkdir -p /etc/ldap
sudo chown $BATOU_USER /etc/ldap
sudo ln -sf /etc/nixos/goibhnix/containers/star/nixos/configuration.nix /etc/nixos/configuration.nix

hg clone https://code.gocept.com/hg/private/webwork.deployment
cd webwork.deployment
BATOU_ROOT=`pwd`

PY26ENVS=(deliverance solr zeo zope)
for PY26ENV in ${PY26ENVS[*]};
do
    mkdir -p work/$PY26ENV/lib/python2.6/site-packages
    ln -sf /run/current-system/sw/lib/python2.6/site-packages/_ssl.so work/$PY26ENV/lib/python2.6/site-packages/
    virtualenv --python python2.6 work/$PY26ENV 
    echo $PY26ENV    
done

PY27ENVS=(supervisor brandbook)
for PY27ENV in ${PY27ENVS[*]};
do
    mkdir -p work/$PY27ENV
    virtualenv  --python python2.7 work/$PY27ENV
    work/$PY27ENV/bin/pip install setuptools==0.9.8
    PYTHONPATH= work/$PY27ENV/bin/pip install distribute==0.6.49
    ln -sf /run/current-system/sw/bin/python2.7-config work/$PY27ENV/bin/python-config
done

virtualenv --python=python2.7 .
bin/python2.7 bootstrap.py
bin/buildout
bin/batou-local dev-cillian-container localhost

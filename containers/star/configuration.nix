{ config, pkgs, ... }:

with pkgs.lib;

let
  options = import ../options.nix {inherit pkgs;};
  name = "star";
in
{
  imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  networking.extraHosts = ''
    10.233.8.2 star.eithniu.eu dummy.eithniu.eu test.star.eithniu.eu 
    127.0.0.1 local.eithniu.eu localhost.eithniu.eu
  '';
  networking.nameservers = [ "8.8.8.8" ];
  users.extraUsers.star = options.userBase // { home = "/srv/${name}"; };

  services = {
    # This is for the whiteside
    # varnish = {
    #   enable = true;
    #   config = builtins.readFile /srv/star/build/webwork.deployment/work/_/etc/varnish/default.vcl;
    # };
    redis.enable = true;
    
    memcached.enable = true;

    # cron = {
    #   cronFiles = [
    #     /srv/star/build/webwork.deployment/work/crontab/crontab
    #   ];
    # };
    
    nginx = {
      enable = true;
      config = ''
        worker_processes 2;
        events {
         worker_connections 1024;
        }

        http {
          # NOTE: this is a modified copy of the batou generated one
          include /srv/star/build/webwork.deployment/work/_/etc/nginx/local/portal-override.conf;
          include /srv/star/build/webwork.deployment/work/_/etc/nginx/local/portal.test.conf;
          include /srv/star/build/webwork.deployment/work/_/etc/nginx/local/brandbook.conf;
          include /srv/star/build/webwork.deployment/work/_/etc/nginx/local/hottopics.conf;
        }
      '';
    };

    openldap = {
      enable = true;
      extraConfig = ''
        include    ${pkgs.openldap}/etc/schema/core.schema
        include    ${pkgs.openldap}/etc/schema/cosine.schema
        include    ${pkgs.openldap}/etc/schema/nis.schema
        include    ${pkgs.openldap}/etc/schema/inetorgperson.schema
        include    ${pkgs.openldap}/etc/schema/corba.schema
        include    ${pkgs.openldap}/etc/schema/duaconf.schema
        include    ${pkgs.openldap}/etc/schema/dyngroup.schema
        include    ${pkgs.openldap}/etc/schema/java.schema
        include    ${pkgs.openldap}/etc/schema/misc.schema
        include    ${pkgs.openldap}/etc/schema/collective.schema
        include    ${pkgs.openldap}/etc/schema/ppolicy.schema
        include    /srv/star/build/webwork.deployment-p8/work/_/etc/openldap/schema/starallianceperson.schema

        # Allow LDAPv2 client connections.  This is NOT the default.
        allow bind_v2
        loglevel none
        sizelimit 10000
        
        moduleload memberof.so
        moduleload ppolicy.so

        # include    /srv/star/build/webwork.deployment/work/_/etc/openldap/slapd.20main-local.conf

        moduleload ppolicy.la
        moduleload back_bdb.so
        moduleload back_monitor.so

        '' + builtins.readFile ./openldap.secrets;
      };
    
  };

  environment = {
    systemPackages = with pkgs; [
      aespipe file mercurial nodejs openjdk openldap.dev postgresql sqlite gnupg gcc pkgconfig
       python27Full pcre gnumake ldapvi lsof ruby exempi poppler poppler_utils lynx
      imagemagick graphicsmagick ghostscript pdftk libreoffice openjdk wkhtmltopdf libffi.dev
    ] ++ options.plonePackages;
    #python26Full python26Star  rubyLibs.jekyll rubyLibs.sass rubyLibs.bundler rubyLibs.jekyll_sass_converter
    pathsToLink = [ "/include" "/libexec" ];
    shellInit = ''
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      export LD_LIBRARY_PATH=${pkgs.exempi}/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.openldap}/include:${pkgs.cyrus_sasl.dev}/include/sasl:${pkgs.postgresql}/include
      export ZSERVER_HOST=${name}
      export DIAZO_ALWAYS_CACHE_RULES=1
    '';
  };
  
  #nixpkgs.config.allowBroken = true; # python2.6

}

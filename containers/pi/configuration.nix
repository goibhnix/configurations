{ config, pkgs, ... }:

with pkgs.lib;
let
  options = import ../options.nix {inherit pkgs;};
  name = "pi";
in

{ imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  networking.extraHosts = ''
      10.233.13.2 pi
  '';
  users.extraUsers.pi = options.userBase // { home = "/srv/${name}"; };
  services = {
    redis.enable = true;
    memcached.enable = true;
    rabbitmq = {
      enable = true;
      plugins = [
        "rabbitmq_management"
        "rabbitmq_web_stomp"
      ];
      configItems = {
        "loopback_users" = "none";
      };
    };
  };
  
  #virtualisation.docker.enable = true;
  environment = {
    systemPackages = with pkgs; [
      gnumake nodejs ruby_2_5 enchant enchant.dev libffi.dev libffi.out mercurial
      python27Packages.docutils file imagemagick graphicsmagick poppler_utils
      ghostscript pdftk libreoffice.libreoffice openjdk wkhtmltopdf geckodriver
      openldap stdenv subversionClient openconnect wv
      python38Full pre-commit python2 ffmpegthumbnailer keychain
      openssl.dev openssl.out

    ] ++ options.plone3Packages;
    # ln -sf /run/current-system/sw/lib/libreoffice /usr/lib/
    #  gem install --user-install bundle
    # export PATH=/srv/pi/.gem/ruby/2.1.0/bin:$PATH
    # bundle install --path /srv/pi/.gem --binstubs
    # bundler
    pathsToLink = [ "/include" ];

    shellInit = ''
      export LD_LIBRARY_PATH=${pkgs.enchant}/lib:${pkgs.glib.out}/lib:${pkgs.pango.out}/lib:${pkgs.fontconfig.lib}/lib
      export ZSERVER_HOST=10.233.13.2
      export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant.dev}/include:${pkgs.zlib}/include:${pkgs.openldap.dev}/include:${pkgs.cyrus_sasl.dev}/include/sasl
      export DIAZO_ALWAYS_CACHE_RULES=1
      export OFFICE_PATH=/run/current-system/sw/bin/soffice
    '';

  };
}

{
  inputs.extra-container.url = "github:erikarvstedt/extra-container";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";

  outputs = { extra-container, ... }@inputs:
    extra-container.inputs.flake-utils.lib.eachSystem extra-container.lib.supportedSystems (system: {
      packages.default = extra-container.lib.buildContainers {
        # The system of the container host
        inherit system;

        # Optional: Set nixpkgs.
        # If unset, the nixpkgs input of extra-container flake is used
        nixpkgs = inputs.nixpkgs;

        # Set this to disable `nix run` support
        # addRunner = false;

        config = {
          containers.pi = {
            privateNetwork = true;
            hostAddress = "10.250.0.1";
            localAddress = "10.233.13.2";
            bindMounts = {
              "/srv" = {
                hostPath = "/srv";
                isReadOnly = false;
              };
              "/home/goibhniu" = {
                hostPath = "/home/goibhniu";
                isReadOnly = false;
              };
            };
            extra = {
              enableWAN = true;
              exposeLocalhost = true;
              firewallAllowHost = true;
            };

            config = { pkgs, ... }:
            let
              options = import ../options.nix {inherit pkgs;};
              name = "pi";
            in
            {
              users.extraUsers.pi = options.userBase // {
                home = "/srv/${name}";
              };
              services = {
                openssh.enable = true;
                redis.servers."".enable = true;
                memcached.enable = true;
                rabbitmq = {
                  enable = true;
                  plugins = [
                    "rabbitmq_management"
                    "rabbitmq_web_stomp"
                  ];
                  configItems = {
                    "loopback_users" = "none";
                  };
                };
              };

              environment = {
                systemPackages = with pkgs; [
                  gnumake nodejs ruby enchant enchant.dev libffi.dev libffi.out 
                  file imagemagick graphicsmagick poppler_utils
                  ghostscript pdftk libreoffice.libreoffice
                  openjdk
                  geckodriver
                  openldap stdenv openconnect wv
                  python38Full pre-commit ffmpegthumbnailer keychain
                  openssl.dev openssl.out
                ] ++ options.plone3Packages;
                # ln -sf /run/current-system/sw/lib/libreoffice /usr/lib/
                #  gem install --user-install bundle
                # export PATH=/srv/pi/.gem/ruby/2.1.0/bin:$PATH
                # bundle install --path /srv/pi/.gem --binstubs
                # bundler
                pathsToLink = [ "/include" ];
                shellInit = ''
                  export LD_LIBRARY_PATH=${pkgs.enchant}/lib:${pkgs.glib.out}/lib:${pkgs.pango.out}/lib:${pkgs.fontconfig.lib}/lib
                  export ZSERVER_HOST=10.233.13.2
                  export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
                  export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant.dev}/include:${pkgs.zlib}/include:${pkgs.openldap.dev}/include:${pkgs.cyrus_sasl.dev}/include/sasl
                  export DIAZO_ALWAYS_CACHE_RULES=1
                  export OFFICE_PATH=/run/current-system/sw/bin/soffice
                '';
              };
            };
          };
        };
      };
    });
}

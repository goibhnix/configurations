{ pkgs, ... }:

with pkgs;
let
plone-python = python2.withPackages (ps: with ps; [ pymysql mysqlclient mysql-connector]);
in
{
  userBase = {  
    createHome = true;
    description = "Dev Account";
    extraGroups = [ "wheel" ];
    isNormalUser = true;
    uid = 1000;
    useDefaultShell = true;
    openssh.authorizedKeys.keys = [ 
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDpk8xm/U3GXLlbhNhEV97uhO5OLFmDJSiVj5mSmNPk0AKMcRqmMtFADlc4PyxO1bU9MCLF08vBiPMCuU7ug1IfEBUdUEiHRozUq/PjzuVd1Ob2O19Lur7c3EXgLYp4FyM1XPL50dGOv3Ki3s5iphUVtJvCPJXz5Kck1JSfKk9QpOQ4TC4IwJlmS5BVmyBg78BN3eyAYq5mAiw8gbiDy1916Vqfodb8FWt+loT6DSFKVW0ax51iycAm4PbEilgA0dOWMn8Sb8my4roprksgt1GX+WIx23ODLWmBQYRGhcTWXoXIrsjm9L288Z//VFh6xGd/KfgUYG6NgE6ZvOC9NiwZ goibhniu@goibhniu"
    ];
  };

#libxslt.dev
  # common packages used in plone projects
  plonePackages = with pkgs; [
     ctags cyrus_sasl.dev gettext git gnupg idutils libjpeg.out libjpeg.dev
     libxml2.dev libxslt.dev  libzip.dev plone-python
     python27Packages.virtualenv
     readline.dev screen sqlite.dev symlinks stdenv wget zlib.dev zlib.out gcc pkgconfig
     which
  ]; #python27Packages.ipythonLight

  plone3Packages = with pkgs; [
      ctags cyrus_sasl.dev gettext git gnupg idutils libjpeg.out libjpeg.dev
      libxml2.dev libxslt.dev  libzip.dev python38Full python3Packages.docutils
      python3Packages.virtualenv
      readline.dev screen sqlite.dev symlinks stdenv wget zlib.dev zlib.out gcc pkgconfig
      which pinentry
  ];
  
}

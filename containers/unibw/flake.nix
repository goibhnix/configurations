{
  inputs.extra-container.url = "github:erikarvstedt/extra-container";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";

  outputs = { extra-container, ... }@inputs:
    extra-container.inputs.flake-utils.lib.eachSystem extra-container.lib.supportedSystems (system: {
      packages.default = extra-container.lib.buildContainers {
        # The system of the container host
        inherit system;

        # Optional: Set nixpkgs.
        # If unset, the nixpkgs input of extra-container flake is used
        nixpkgs = inputs.nixpkgs;

        # Set this to disable `nix run` support
        # addRunner = false;

        config = {
          containers.unibw-py3 = {
            privateNetwork = true;
            hostAddress = "10.250.0.1";
            localAddress = "10.233.21.2";
            bindMounts = {
              "/srv" = {
                hostPath = "/srv";
                isReadOnly = false;
              };
              "/home/goibhniu" = {
                hostPath = "/home/goibhniu";
                isReadOnly = false;
              };
            };
            extra = {
              enableWAN = true;
              exposeLocalhost = true;
              firewallAllowHost = true;
            };

            config = { pkgs, ... }:
            let
              options = import ../options.nix {inherit pkgs;};
              name = "unibw-py3";
            in
            {
              users.extraUsers.unibw-py3 = options.userBase // {
                home = "/srv/${name}";
              };
              services = {
                openssh.enable = true;
                redis.servers."".enable = true;
                memcached.enable = true;
              };

              environment = {
                systemPackages = with pkgs; [
                  cyrus_sasl gnumake nodejs ruby  enchant
                  libffi.dev libffi.out binutils-unwrapped
                  openldap openldap.dev openconnect file python2 postgresql
                  graphicsmagick poppler ghostscript pdftk elinks gnupg
                  mercurial openjdk subversion pcre.dev poppler_utils unzip zlib mysql.out mysql
                  openssl.dev openssl.out ansible
                  pinentry_qt5 jekyll
                ] ++ options.plone3Packages;
                # libreoffice 
                # ln -sf /run/current-system/sw/lib/libreoffice /usr/lib/
                # tests fail because collective.documentviewer calls soffice like this:
                # SYSUSERCONFIG=file:///srv/ikath/build/ttt/tmp /usr/lib/libreoffice/program/soffice --headless --invisible  --norestore --nolockcheck --convert-to pdf --outdir ./tmp/ /tmp/tmpsVNd4K/test-file/dump.dat
                # which creates tmp/libreofficedev, but collective.documentviewer
                # expects there to be tmp/libreoffice, and removes that instead, could be a nix thing
                # see: .gem/ruby/2.1.0/gems/docsplit-0.7.6/lib/docsplit/pdf_extractor.rb
                # editing collective/documentviewer/convert.py and removing
                # libreofficedev instead of libreoffice "fixes" this
                
                # modify the makefile and use the system jekyll, nokogiri fails to install
                pathsToLink = [ "/include" ];
                shellInit = ''
                  export LD_LIBRARY_PATH=${pkgs.enchant}/lib:${pkgs.openssl.out}/lib:${pkgs.glib.out}/lib:${pkgs.pango.out}/lib:${pkgs.fontconfig.lib}/lib
                  export PLONE_CSRF_DISABLED=true
                  export ZSERVER_HOST=10.233.21.2
                  export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
                  export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant}/include:${pkgs.cyrus_sasl.dev}/include/sasl:/var/run/current-system/sw/include/mysql
                '';
              };
            };
          };
        };
      };
    });
}

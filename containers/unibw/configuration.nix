{ config, pkgs, ... }:

with pkgs.lib;
let
  options = import ../options.nix {inherit pkgs;};
  server = import ../../server.nix {inherit pkgs;};
  name = "unibw-py3";
in

{ imports = [ ../base.nix ];
  networking.hostName = mkDefault name;
  networking.extraHosts = ''
      10.233.21.2 unibw-py3.eithniu.eu 
  '';
  networking.nameservers = [ "8.8.8.8" ];
  networking.interfaces = {
    tun0 = {
      virtual = true;
      virtualType = "tun";
    };
  };
  
  # security.acme = {
  #   certs."testnixcert.quaivecloud.com" = {
  #     webroot = "/srv/unibw-py3/acme";
  #     email = "deroiste@syslab.com";
  #     allowKeysForGroup = true;
  #     directory = "/srv/unibw-py3/acme";
  #   };
  # };

  users.extraUsers.unibw = options.userBase // { home = "/srv/${name}"; name = "${name}";};
  #systemd.services.redis.serviceConfig.LimitNOFILE=10032;
  services.redis.enable = true;
  # services.redis.logLevel = "verbose";
  # services.redis.logfile = "/var/log/redis.log";
  services.memcached.enable = true;
  environment = {
    systemPackages = with pkgs; [
      cyrus_sasl gnumake nodejs ruby  enchant
      libffi.dev libffi.out binutils-unwrapped
       openldap openldap.dev openconnect file python2 postgresql
      graphicsmagick poppler ghostscript pdftk elinks gnupg
      mercurial openjdk subversion pcre.dev poppler_utils unzip zlib mysql.out mysql.dev mysql
      openssl.dev openssl.out
      pinentry_qt5 jekyll
    ] ++ options.plone3Packages;
    # libreoffice 
    # ln -sf /run/current-system/sw/lib/libreoffice /usr/lib/
    # tests fail because collective.documentviewer calls soffice like this:
    # SYSUSERCONFIG=file:///srv/ikath/build/ttt/tmp /usr/lib/libreoffice/program/soffice --headless --invisible  --norestore --nolockcheck --convert-to pdf --outdir ./tmp/ /tmp/tmpsVNd4K/test-file/dump.dat
    # which creates tmp/libreofficedev, but collective.documentviewer
    # expects there to be tmp/libreoffice, and removes that instead, could be a nix thing
    # see: .gem/ruby/2.1.0/gems/docsplit-0.7.6/lib/docsplit/pdf_extractor.rb
    # editing collective/documentviewer/convert.py and removing
    # libreofficedev instead of libreoffice "fixes" this
    
    # modify the makefile and use the system jekyll, nokogiri fails to install
    shellInit = ''
      export LD_LIBRARY_PATH=${pkgs.enchant}/lib:${pkgs.openssl.out}/lib
      export PLONE_CSRF_DISABLED=true
      export ZSERVER_HOST=10.233.21.2
      export LIBRARY_PATH=/var/run/current-system/sw/lib:${pkgs.enchant}/lib
      export C_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant}/include:${pkgs.cyrus_sasl.dev}/include/sasl:/var/run/current-system/sw/include/mysql
      export CPLUS_INCLUDE_PATH=/var/run/current-system/sw/include:${pkgs.enchant}/include:${pkgs.cyrus_sasl.dev}/include/sasl:/var/run/current-system/sw/include/mysql
    '';

  };
}

{ config, lib, pkgs, ... }:

let
  # https://svn.nixos.org/repos/nix/configurations/trunk/misc/raskin
  packageGroups = import ../../package-groups.nix {inherit pkgs;};
in 

with lib;

{ boot.isContainer = true;
  security.initialRootPassword = mkDefault "!";
  networking = {
    hostName = mkDefault "elzemieke";
    useDHCP = false;
    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 443 2222 4369 8000 8443 ];
      extraCommands = ''
        iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to 8000
        iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to 8443
        iptables -t nat -A OUTPUT -p tcp -d 37.221.197.163 --dport 80 -j REDIRECT --to 8000
        iptables -t nat -A OUTPUT -p tcp -d 37.221.197.163 --dport 443 -j REDIRECT --to 8443
      '';
    };
  };


  services = {
    openssh.enable = true;
    postgresql = {
      enable = true;
      package = pkgs.postgresql92;
      # createuser zotonic
      # createdb elzemieke
      # psql elzemieke < backups/...
      # psql
      # alter user zotonic with password 'blah';
    };

  };  

  users = {
    mutableUsers = true;
    extraUsers.zotonic = {
      createHome = true;
      home = "/srv/zotonic";
      description = "Dev account";
      extraGroups = [ "wheel" ];
      uid = 1001;
      useDefaultShell = true;
      openssh.authorizedKeys.keys = [ 
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKvsA2hFOGSx+kys6U7NxKZVRlmbMhUkp3ceJTPaqR/L3u0RsZJragktGUVVt7cdNg1QYRCE4VTJv4z3G2Pv+ZLuCV/HDBHAq63JfcKF7IzQkZfW77Ac+ulrWmbC6TBfYIGcppJ+XMdkn/sF3X1XiVheFdKY2VMdjmvHZjkhuYD8XQFM5EwYq2bSluZ0rhrzIwOO4ZTBa54/wGP1YDzOdJe60XLwj0zcUfxnCa0TrHoQFGcL6I82gXrKBEy3bOqfC1yB0BtgDXyDLBSv/BTVH1nOtD/Ur2ufgP7H7pcO5hbKsr4+Xk9upjrIxhdx3TA5qTwF4XnrawI9vIhsZbUl13 goibhniu@goibhniu"
      ];
    };
  };

  environment.systemPackages = with packageGroups; [ zotonic pkgs.git ];

  system.replaceRuntimeDependencies = with pkgs; [
    {
      original = bash;
      replacement = pkgs.lib.overrideDerivation bash (oldAttrs: {
        patches = oldAttrs.patches ++ [
          (fetchurl {
            url = "mirror://gnu/bash/bash-4.2-patches/bash42-048";
            sha256 = "091xk1ms7ycnczsl3fx461gjhj69j6ycnfijlymwj6mj60ims6km";
          })
          (fetchurl {
            url = "mirror://gnu/bash/bash-4.2-patches/bash42-049";
            sha256 = "1d2ympd8icz3q3kbsf2d8inzqpv42q1yg12kynf316msiwxdca3z";
          })
          (fetchurl {
            url = "mirror://gnu/bash/bash-4.2-patches/bash42-050";
            sha256 = "19lb9nh0x5siwf21xkga3khy5pa3srfrlx97mby4cfz8am2bh68s";
          })
        ];
      });
    }
    {
      original = bashInteractive;
      replacement = pkgs.lib.overrideDerivation bashInteractive (oldAttrs: {
        patches = oldAttrs.patches ++ [
          (fetchurl {
            url = "mirror://gnu/bash/bash-4.2-patches/bash42-048";
            sha256 = "091xk1ms7ycnczsl3fx461gjhj69j6ycnfijlymwj6mj60ims6km";
          })
          (fetchurl {
            url = "mirror://gnu/bash/bash-4.2-patches/bash42-049";
            sha256 = "1d2ympd8icz3q3kbsf2d8inzqpv42q1yg12kynf316msiwxdca3z";
          })
          (fetchurl {
            url = "mirror://gnu/bash/bash-4.2-patches/bash42-050";
            sha256 = "19lb9nh0x5siwf21xkga3khy5pa3srfrlx97mby4cfz8am2bh68s";
          })
        ];
      });
    }
  ];

}

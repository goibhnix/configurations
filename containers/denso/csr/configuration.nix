{ config, pkgs, ... }:
let
  python27 = pkgs.python27Full.override {
    extraLibs = with pkgs.python27Packages; [
    ];
  };
in

with pkgs.lib;

{ boot.isContainer = true;
  security.initialRootPassword = mkDefault "!";
  networking = {
    hostName = mkDefault "csr";
    useDHCP = false;
    firewall.enable = false;
    extraHosts = ''
      127.0.0.1 smartprintng
    '';
  };

  services = {
    openssh.enable = true;
  };

  users.extraUsers.denso = {
    createHome = true;
    home = "/srv/denso";
    description = "Dev Account";
    extraGroups = [ "wheel" ];
    uid = 1000;
    useDefaultShell = true;
    openssh.authorizedKeys.keys = [ 
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDpk8xm/U3GXLlbhNhEV97uhO5OLFmDJSiVj5mSmNPk0AKMcRqmMtFADlc4PyxO1bU9MCLF08vBiPMCuU7ug1IfEBUdUEiHRozUq/PjzuVd1Ob2O19Lur7c3EXgLYp4FyM1XPL50dGOv3Ki3s5iphUVtJvCPJXz5Kck1JSfKk9QpOQ4TC4IwJlmS5BVmyBg78BN3eyAYq5mAiw8gbiDy1916Vqfodb8FWt+loT6DSFKVW0ax51iycAm4PbEilgA0dOWMn8Sb8my4roprksgt1GX+WIx23ODLWmBQYRGhcTWXoXIrsjm9L288Z//VFh6xGd/KfgUYG6NgE6ZvOC9NiwZ goibhniu@goibhniu"
    ];
  };

  fonts = {
    enableCoreFonts = true;
    enableFontConfig = true;
  };

  environment = {
    systemPackages = with pkgs; [
      corefonts
      git
      gcc
      libxml2
      libxslt
      libzip
      lsof
      openjdk
      python27Full
      python27Packages.virtualenv
      python27Packages.ipythonLight
      readline
      screen
      stdenv
      symlinks
      wget
      zlib
      unzip
    ];

    shellInit = ''
      export LIBRARY_PATH=/var/run/current-system/sw/lib
      export C_INCLUDE_PATH=${pkgs.readline}/include
    '';
  };
}

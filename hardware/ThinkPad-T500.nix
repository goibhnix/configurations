# This is a generated file.  Do not modify!
# Make changes to /etc/nixos/configuration.nix instead.
{ config, pkgs, modulesPath, ... }:

{
  require = [
    "${modulesPath}/installer/scan/not-detected.nix"
  ];

  boot.initrd.kernelModules = [ "uhci_hcd" "ehci_hcd" "ahci" "ohci1394" "usb_storage" ];
  boot.kernelModules = [ "acpi-cpufreq" "kvm-intel" "snd-seq" "snd-rawmidi" ];
  boot.extraModulePackages = [ ];

  nix.maxJobs = 1;
  services.xserver.videoDrivers = ["intel" "ati"];
  services.thinkfan.enable = true;
}

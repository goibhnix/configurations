# This is a generated file.  Do not modify!
# Make changes to /etc/nixos/configuration.nix instead.
{ config, pkgs, modulesPath, ... }:

{
  require = [
    "${modulesPath}/profiles/base.nix"
    "${modulesPath}/installer/scan/not-detected.nix"
  ];

  boot.initrd.kernelModules = [ "ahci" "ohci_hcd" "ehci_hcd" "sdhci_pci" "uas" "sd_mod" "rtsx_pci_sdmmc" "pata_atiixp" "usb_storage" "usbhid" ]; #"xhci" 
  boot.kernelModules = [ "kvm-amd" "kvm-intel" "tun" "fuse" "virtio" "snd-seq" "snd-rawmidi"];
  boot.extraModulePackages = [ ];

  nix.settings.max-jobs = 6;
}

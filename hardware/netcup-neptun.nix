{ config, pkgs, ... }:
let
  common = import ../common.nix {inherit pkgs;};
in
{
  require = [
    <nixos/modules/installer/scan/not-detected.nix>
  ];

  boot = common.boot // {
    initrd.kernelModules = [
      "ata_piix" "uhci_hcd" "virtio_blk" "virtio_pci"
    ];
    kernelModules = [ ];
    extraModulePackages = [ ];
    loader.grub = common.grub // { device = "/dev/vda";};
  };

  fileSystems."/".label = "root";
  swapDevices = [{ label = "swap"; }];

  nix.maxJobs = 2;
}

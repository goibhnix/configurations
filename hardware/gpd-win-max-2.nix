{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "thunderbolt" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/d05a121a-a5db-43ee-87de-31397af14e44";
      fsType = "btrfs";
      options = [ "subvol=root" ];
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/d05a121a-a5db-43ee-87de-31397af14e44";
      fsType = "btrfs";
      options = [ "subvol=home" ];
    };

  fileSystems."/nix" =
    { device = "/dev/disk/by-uuid/d05a121a-a5db-43ee-87de-31397af14e44";
      fsType = "btrfs";
      options = [ "subvol=nix" "noatime" ];
    };

  fileSystems."/var/lib" =
    { device = "/dev/disk/by-uuid/d05a121a-a5db-43ee-87de-31397af14e44";
      fsType = "btrfs";
      options = [ "subvol=var/lib" ];
    };

  # fileSystems."/swap" =
  #   { device = "/dev/disk/by-uuid/d05a121a-a5db-43ee-87de-31397af14e44";
  #     fsType = "btrfs";
  #     options = [ "subvol=swap" "noatime" ];
  #   };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/F20F-2E95";
      fsType = "vfat";
    };

  swapDevices = [ { device = "/dev/disk/by-uuid/a0bb4453-e8a7-4012-ad8f-23ed8d5611d6"; } ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp1s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  # high-resolution display
  # hardware.video.hidpi.enable = lib.mkDefault true;
  systemd.sleep.extraConfig = "HibernateMode=shutdown";
}

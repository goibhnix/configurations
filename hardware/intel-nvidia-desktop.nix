# This is a generated file.  Do not modify!
# Make changes to /etc/nixos/configuration.nix instead.
{ config, pkgs, modulesPath, ... }:

{
  require = [
    "${modulesPath}/profiles/base.nix"
    "${modulesPath}/installer/scan/not-detected.nix"
  ];

  boot.initrd.kernelModules = [ "ahci" "ohci_hcd" "ehci_hcd" "pata_atiixp" "xhci" "usb_storage" "usbhid" ];
  boot.kernelModules = [ "kvm-intel" "tun" "fuse" "virtio" ]; # "snd-seq" "snd-rawmidi"
  boot.extraModulePackages = [ ];

  nix.maxJobs = 6;
}
